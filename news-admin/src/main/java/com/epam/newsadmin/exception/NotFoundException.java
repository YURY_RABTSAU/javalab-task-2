package com.epam.newsadmin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)//for 404
public class NotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NotFoundException() {
		super();
	}
	
	public NotFoundException(String s) {
		super(s);
	}
	
	public NotFoundException(Throwable t) {
		super(t);
	}
	
	public NotFoundException(String s, Throwable t) {
		super(s, t);
	}
	
}
