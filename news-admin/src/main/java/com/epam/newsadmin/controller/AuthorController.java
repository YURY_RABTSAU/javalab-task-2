package com.epam.newsadmin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsadmin.exception.ControllerException;
import com.epam.newscommon.entity.AuthorTO;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.AuthorService;

@Controller
public class AuthorController {
	
	private static final Logger LOG = Logger.getLogger(AuthorController.class);
	
	@Autowired
	private AuthorService authorService;
	
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	@RequestMapping(value = Mapping.AUTHORS, method = RequestMethod.GET)
	public String viewAuthors(HttpServletRequest req, Model model) throws Exception {

		try {
			List<AuthorTO> allAuthorList = authorService.getAllAuthors();
			model.addAttribute("allAuthors", allAuthorList);
		} catch (ServiceException e) {
			LOG.error("failed to load authors", e);
			throw new ControllerException("Failed to load authors", e);
		}
		
		return "authorListView";
	}
	
	@RequestMapping(value = Mapping.AUTHOR_CREATE, method = RequestMethod.POST)
	public String createAuthor(
			@RequestParam(value = "author-name") String authorName) throws Exception {
		
		AuthorTO author = new AuthorTO();
		author.setName(authorName);
		
		try {
			authorService.createAuthor(author);
		} catch (ServiceException e) {
			LOG.error("could not create author", e);
			throw new ControllerException(e);
		}
		
		return "redirect:" + Mapping.AUTHORS;
	}
	
	@RequestMapping(value = Mapping.AUTHOR_UPDATE, method = RequestMethod.POST)
	public String updateAuthor(
			@RequestParam(value = "author-name") String authorName,
			@RequestParam(value = "author-id") Long authorId) throws Exception {
		
		try {
			AuthorTO author = authorService.getAuthorById(authorId);
			author.setName(authorName);
			authorService.updateAuthor(author);
		} catch (ServiceException e) {
			LOG.error("could not update author", e);
			throw new ControllerException(e);
		}
		
		return "redirect:" + Mapping.AUTHORS;
	}
	
	@RequestMapping(value = Mapping.AUTHOR_EXPIRE, method = RequestMethod.POST)
	public String expireAuthor(
			@RequestParam(value = "author-id") Long authorId) throws Exception {
		
		try {
			AuthorTO author = authorService.getAuthorById(authorId);
			author.setExpired(new Date());
			authorService.updateAuthor(author);
		} catch (ServiceException e) {
			LOG.error("could not expire author", e);
			throw new ControllerException(e);
		}
		
		return "redirect:" + Mapping.AUTHORS;
	}

}
