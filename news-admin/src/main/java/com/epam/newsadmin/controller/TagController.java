package com.epam.newsadmin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsadmin.exception.ControllerException;
import com.epam.newscommon.entity.TagTO;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.TagService;

@Controller
public class TagController {
	
	private static final Logger LOG = Logger.getLogger(TagController.class);

	@Autowired
	private TagService tagService;
	
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	@RequestMapping(value = Mapping.TAGS, method = RequestMethod.GET)
	public String viewTags(HttpServletRequest req, Model model) throws Exception {

		try {
			List<TagTO> allTagList = tagService.getAllTags();
			model.addAttribute("allTags", allTagList);
		} catch (ServiceException e) {
			LOG.error("failed to load tags", e);
			throw new ControllerException("Failed to load tags", e);
		}
		
		return "tagListView";
	}
	
	@RequestMapping(value = Mapping.TAG_CREATE, method = RequestMethod.POST)
	public String createTag(
			@RequestParam(value = "tag-name") String tagName) throws Exception {
		
		TagTO tag = new TagTO();
		tag.setName(tagName);
		
		try {
			tagService.createTag(tag);
		} catch (ServiceException e) {
			LOG.error("could not create tag", e);
			throw new ControllerException(e);
		}
		
		return "redirect:" + Mapping.TAGS;
	}
	
	@RequestMapping(value = Mapping.TAG_UPDATE, method = RequestMethod.POST)
	public String updateTag(
			@RequestParam(value = "tag-name") String tagName,
			@RequestParam(value = "tag-id") Long tagId) throws Exception {
		
		TagTO tag = new TagTO();
		tag.setId(tagId);
		tag.setName(tagName);
		
		try {
			tagService.updateTag(tag);
		} catch (ServiceException e) {
			LOG.error("could not update tag", e);
			throw new ControllerException(e);
		}
		
		return "redirect:" + Mapping.TAGS;
	}
	
	@RequestMapping(value = Mapping.TAG_DELETE, method = RequestMethod.POST)
	public String deleteTag(
			@RequestParam(value = "tag-id") Long tagId) throws Exception {
		
		try {
			tagService.deleteTag(tagId);
		} catch (ServiceException e) {
			LOG.error("could not delete tag", e);
			throw new ControllerException(e);
		}
		
		return "redirect:" + Mapping.TAGS;
	}
	
}
