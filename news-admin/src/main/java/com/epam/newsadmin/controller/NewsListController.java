package com.epam.newsadmin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsadmin.exception.ControllerException;
import com.epam.newscommon.entity.AuthorTO;
import com.epam.newscommon.entity.NewsVO;
import com.epam.newscommon.entity.SearchCriteria;
import com.epam.newscommon.entity.TagTO;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.AuthorService;
import com.epam.newscommon.service.NewsManager;
import com.epam.newscommon.service.NewsService;
import com.epam.newscommon.service.TagService;

@Controller
public class NewsListController {

	private static final Logger LOG = Logger.getLogger(NewsListController.class);
	
	@Autowired
	private NewsManager newsManager;
	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	
	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	@RequestMapping(value = {Mapping.HOME, Mapping.ROOT} )
	public String goHome(HttpServletRequest req) {
		HttpSession session = req.getSession();
		SearchCriteria searchCriteria = ControllerUtils.getDefaultSearchCriteria();
		session.setAttribute("search-criteria", searchCriteria);

		return "redirect:" + Mapping.NEWS;
	}

	@RequestMapping(value = Mapping.NEWS)
	public String getNewsList(HttpServletRequest req, Model model,
			@RequestParam(value = "page", required = false) Integer pageNumber)
			throws ControllerException {
		SearchCriteria searchCriteria = ControllerUtils.getSearchCriteria(req);

		if (pageNumber == null || pageNumber < 1) {
			pageNumber = 1;
		}
		LOG.debug("requested news list, page = " + pageNumber + ", " + searchCriteria);

		try {
			addNewsByPage(searchCriteria, model, pageNumber);
			addMenuElements(model, searchCriteria);
		} catch (ServiceException e) {
			throw new ControllerException("Failed to load news", e);
		}
		return "newsListView";
	}

	

	@RequestMapping(value = Mapping.SEARCH, method = RequestMethod.POST)
	public String setSearch(HttpServletRequest req, Model model,
			@RequestParam(value = "act") String action,
			@RequestParam(value = "select-author") Long authorId,
			@RequestParam(value = "tag-checkbox", required = false) Long[] tagIds) {
		HttpSession session = req.getSession();
		SearchCriteria newCriteria = ControllerUtils.getDefaultSearchCriteria();

		switch (action) {
		case "dropSearch":
			break;

		default:
			/* falls through */

		case "setSearch":
			newCriteria.setAuthorId(authorId);
			if(tagIds != null) {
				for (int i = 0; i < tagIds.length; i++) {
					newCriteria.addTag(tagIds[i]);
				}
			}
			break;
		}

		session.setAttribute("search-criteria", newCriteria);
		return "redirect:" + Mapping.NEWS;
	}

	private int getNumberOfResults(SearchCriteria searchCriteria)
			throws ServiceException {
		SearchCriteria criteriaCopy = searchCriteria.copy();
		criteriaCopy.dropOffset();
		criteriaCopy.dropLimit();
		return newsService.getNews(criteriaCopy).size();
	}

	private void addNewsByPage(SearchCriteria criteria, Model model,
			int pageNumber) throws ServiceException {
		
		int numberOfNews = getNumberOfResults(criteria);
		int numberOfPages = numberOfNews / criteria.getLimit();
		if (numberOfPages * criteria.getLimit() < numberOfNews) {
			numberOfPages++;
		}
		model.addAttribute("pagesCount", numberOfPages);
		model.addAttribute("pageNumber", pageNumber);

		int offset = (pageNumber - 1) * criteria.getLimit();
		SearchCriteria offsetCriteria = criteria.copy();
		offsetCriteria.setOffset(offset);

		List<NewsVO> newsList = newsManager.getNews(offsetCriteria);
		model.addAttribute("newsList", newsList);
	}
	
	private void addMenuElements(Model model, SearchCriteria criteria)
			throws ControllerException {
		
		try {
			List<AuthorTO> allAuthorList = authorService.getAllAuthors();
			model.addAttribute("allAuthors", allAuthorList);
		} catch (ServiceException e) {
			LOG.error("failed to load authors", e);
			throw new ControllerException("Failed to load authors", e);
		}

		try {
			List<TagTO> allTagList = tagService.getAllTags();
			model.addAttribute("allTags", allTagList);
		} catch (ServiceException e) {
			LOG.error("failed to load tags", e);
			throw new ControllerException("Failed to load tags", e);
		}

		model.addAttribute("selectedAuthor", criteria.getAuthorId());
		model.addAttribute("selectedTags", criteria.getTagIds());

	}

}
