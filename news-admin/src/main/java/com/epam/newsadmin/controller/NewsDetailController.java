package com.epam.newsadmin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsadmin.exception.ControllerException;
import com.epam.newsadmin.exception.NotFoundException;
import com.epam.newscommon.entity.CommentTO;
import com.epam.newscommon.entity.NewsVO;
import com.epam.newscommon.entity.SearchCriteria;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.CommentService;
import com.epam.newscommon.service.NewsManager;
import com.epam.newscommon.service.NewsService;

@Controller
public class NewsDetailController {
	
	private static final Logger LOG = Logger.getLogger(NewsDetailController.class);
	
	@Autowired
	private NewsManager newsManager;
	@Autowired
	private NewsService newsService;
	@Autowired
	private CommentService commentService;
	
	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}
	
	@RequestMapping(value = Mapping.NEWS_DETAIL, method = RequestMethod.GET)
	public String getNews(HttpServletRequest req, Model model,
			@RequestParam(value = "id") Long newsId) throws Exception {

		if (newsId < 0) {
			throw new NotFoundException("Invalid newsId: " + newsId);
		}
		SearchCriteria searchCriteria = ControllerUtils.getSearchCriteria(req);

		NewsVO newsVO = newsManager.getNewsById(newsId);
		if (newsVO == null) {
			throw new NotFoundException();
		}
		Long nextId = newsService.getNextNews(searchCriteria, newsId);
		Long prevId = newsService.getPreviousNews(searchCriteria, newsId);
		
		model.addAttribute("newsVO", newsVO);
		model.addAttribute("nextId", nextId);
		model.addAttribute("prevId", prevId);
		
		return "newsDetailView";
	}
	
	@RequestMapping(value = Mapping.SEND_COMMENT, method = RequestMethod.POST)
	public String sendComment(Model model,
			@RequestParam(value = "newsId") Long newsId,
			@RequestParam(value = "commentText") String commentText) {
		
		Date commentDate = new Date();
		CommentTO comment = new CommentTO();
		comment.setCreationDate(commentDate);
		comment.setNewsId(newsId);
		comment.setText(commentText);
		try {
			commentService.createComment(comment);
			LOG.debug("posted comment: " + comment);
		} catch (ServiceException e) {
			LOG.error("failed to add comment", e);
		}
		return "redirect:" + Mapping.NEWS_DETAIL + "?id=" + newsId;
	}
	
	@RequestMapping(value = Mapping.DELETE_COMMENT, method = RequestMethod.POST)
	public String deleteComment(Model model,
			@RequestParam(value = "newsId") Long newsId,
			@RequestParam(value = "commentId") Long commentId) {
		
		try {
			commentService.deleteComment(commentId);
			LOG.debug("deleted comment: id = " + commentId);
		} catch (ServiceException e) {
			LOG.error("failed to delete comment", e);
		}
		return "redirect:" + Mapping.NEWS_DETAIL + "?id=" + newsId;
	}
	
	@RequestMapping(value = Mapping.NEWS_DELETE, method = RequestMethod.POST)
	public String deleteNews(Model model,
			@RequestParam(value = "newsId") Long newsId) throws Exception {
		
		try {
			newsService.deleteNews(newsId);
			LOG.debug("deleted news: id = " + newsId);
		} catch (ServiceException e) {
			LOG.error("failed to delete news", e);
			throw new ControllerException(e);
		}
		return "redirect:" + Mapping.NEWS;
	}

}
