package com.epam.newsadmin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorController {
	
	@RequestMapping(value=Mapping.ERROR_404)
	public String error404(Model model) throws Exception {

		return "404View";
	}
	
	@RequestMapping(value=Mapping.ERROR_500)
	public String error500(Model model) throws Exception {
		
		return "500View";
	}

}
