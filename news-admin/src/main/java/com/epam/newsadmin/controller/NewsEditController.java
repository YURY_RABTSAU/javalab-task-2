package com.epam.newsadmin.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsadmin.exception.ControllerException;
import com.epam.newsadmin.exception.NotFoundException;
import com.epam.newscommon.entity.AuthorTO;
import com.epam.newscommon.entity.NewsTO;
import com.epam.newscommon.entity.NewsVO;
import com.epam.newscommon.entity.TagTO;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.AuthorService;
import com.epam.newscommon.service.NewsManager;
import com.epam.newscommon.service.NewsService;
import com.epam.newscommon.service.TagService;

@Controller
public class NewsEditController {

	private static final Logger LOG = Logger
			.getLogger(NewsEditController.class);

	@Autowired
	private NewsManager newsManager;
	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;

	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	@RequestMapping(value = Mapping.NEWS_CREATE, method = RequestMethod.GET)
	public String getCreateForm(Model model) throws Exception {

		NewsVO newsVO = new NewsVO();
		newsVO.setCreationDate(new Date());
		newsVO.setModificationDate(newsVO.getCreationDate());
		model.addAttribute("newsVO", newsVO);

		try {
			List<AuthorTO> allAuthorList = authorService.getAllAuthors();
			model.addAttribute("allAuthors", allAuthorList);
		} catch (ServiceException e) {
			LOG.error("failed to load authors", e);
			throw new ControllerException("Failed to load authors", e);
		}

		try {
			List<TagTO> allTagList = tagService.getAllTags();
			model.addAttribute("allTags", allTagList);
		} catch (ServiceException e) {
			LOG.error("failed to load tags", e);
			throw new ControllerException("Failed to load tags", e);
		}

		return "newsEditView";

	}

	@RequestMapping(value = {Mapping.NEWS_CREATE, Mapping.NEWS_UPDATE}, 
			method = RequestMethod.POST)
	public String processForm(HttpServletRequest req, Model model,
			@RequestParam(value = "news-title") String title,
			@RequestParam(value = "news-short") String shortText,
			@RequestParam(value = "news-full") String fullText,
			@RequestParam(value = "select-author") Long authorId,
			@RequestParam(value = "tag-checkbox", required = false) Long[] tagIds,
			@RequestParam(value = "id", required = false) Long newsId) 
					throws Exception {

		NewsTO newsTO = new NewsTO();
		NewsVO newsVO = new NewsVO();
		try {
			if (newsId != null) {
				newsTO.setId(newsId);
				newsTO.setModificationDate(new Date());
				//newsVO = newsManager.getNewsById(newsId);
				NewsTO oldNews = newsService.getNewsById(newsId);
				newsTO.setCreationDate(oldNews.getCreationDate());
			} else {
				newsTO.setId(-1);
				newsTO.setCreationDate(new Date());
				newsTO.setModificationDate(newsTO.getCreationDate());
			}
			newsTO.setTitle(title);
			newsTO.setShortText(shortText);
			newsTO.setFullText(fullText);
		
		
		
			AuthorTO authorTO = authorService.getAuthorById(authorId);
			List<TagTO> tagList = new ArrayList<TagTO>();
			if(tagIds != null) {
				for (int i = 0; i < tagIds.length; i++) {
					tagList.add(tagService.getTagById(tagIds[i]));
				}
			}

			newsVO.setNewsTO(newsTO);
			newsVO.setAuthor(authorTO);
			newsVO.setTags(tagList);

			newsManager.addNews(newsVO);
			LOG.debug("news added: " + newsVO);
		} catch (ServiceException e) {
			LOG.error("failed to create/update news", e);
			throw new ControllerException(e);
		}

		return "redirect:" + Mapping.NEWS_DETAIL + "?id=" + newsVO.getId();
	}

	@RequestMapping(value = Mapping.NEWS_UPDATE, method = RequestMethod.GET)
	public String editNews(HttpServletRequest req, Model model,
			@RequestParam(value = "id") Long newsId) throws Exception {

		if (newsId < 0) {
			throw new NotFoundException("Invalid newsId: " + newsId);
		}

		NewsVO newsVO = newsManager.getNewsById(newsId);
		if (newsVO == null) {
			throw new NotFoundException();
		}

		List<Long> newsTags = new ArrayList<Long>();
		for (TagTO tag : newsVO.getTags()) {
			newsTags.add(tag.getId());
		}

		model.addAttribute("newsVO", newsVO);
		model.addAttribute("newsTags", newsTags);

		try {
			List<AuthorTO> allAuthorList = authorService.getAllAuthors();
			model.addAttribute("allAuthors", allAuthorList);
		} catch (ServiceException e) {
			LOG.error("failed to load authors", e);
			throw new ControllerException("Failed to load authors", e);
		}

		try {
			List<TagTO> allTagList = tagService.getAllTags();
			model.addAttribute("allTags", allTagList);
		} catch (ServiceException e) {
			LOG.error("failed to load tags", e);
			throw new ControllerException("Failed to load tags", e);
		}

		return "newsEditView";
	}

}
