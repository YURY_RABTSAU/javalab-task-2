package com.epam.newsadmin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newscommon.entity.SearchCriteria;

public class ControllerUtils {
	
	private static final int DEFAULT_LIMIT = 6;
	
	public static SearchCriteria getSearchCriteria(HttpServletRequest req) {
		HttpSession session = req.getSession();
		SearchCriteria criteria = (SearchCriteria) session
				.getAttribute("search-criteria");
		if (criteria == null) {
			criteria = getDefaultSearchCriteria();
			session.setAttribute("search-criteria", criteria);
		}
		return criteria;
	}
	
	public static SearchCriteria getDefaultSearchCriteria() {
		SearchCriteria searchCriteria = new SearchCriteria();

		searchCriteria.setLimit(DEFAULT_LIMIT);
		return searchCriteria;
	}

}
