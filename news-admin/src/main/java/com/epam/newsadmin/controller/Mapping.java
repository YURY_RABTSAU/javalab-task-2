package com.epam.newsadmin.controller;

public class Mapping {

	public static final String ROOT = "/";
	public static final String HOME = "/home";
	public static final String NEWS = "/news";
	public static final String NEWS_DETAIL = "/newsdetail";
	public static final String SEARCH = "/search";
	public static final String SEND_COMMENT = "/comment";
	public static final String DELETE_COMMENT = "/comment_del";
	
	public static final String NEWS_CREATE = "/news_new";
	public static final String NEWS_UPDATE = "/news_upd";
	public static final String NEWS_DELETE = "/news_del";
	
	public static final String AUTHORS = "/authors";
	public static final String AUTHOR_CREATE = "/author_new";
	public static final String AUTHOR_UPDATE = "/author_upd";
	public static final String AUTHOR_EXPIRE = "/author_exp";
	public static final String TAGS = "/tags";
	public static final String TAG_CREATE = "/tag_new";
	public static final String TAG_UPDATE = "/tag_upd";
	public static final String TAG_DELETE = "/tag_del";
	
	public static final String ERROR_404 = "/404";
	public static final String ERROR_500 = "/500";
	
}
