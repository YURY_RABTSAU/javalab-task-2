/**
 * 
 */
function editShow(id) {
	$(".static-"+id)[0].style.display='none';
	$(".dynamic-"+id)[0].style.display='block';
	$(".dynamic-"+id + " input[name*='-name']").focus();
}

function editHide(id) {
	$(".static-"+id)[0].style.display='block';
	$(".dynamic-"+id)[0].style.display='none';
	$(".dynamic-"+id + " input[name*='-name']").blur();
}
