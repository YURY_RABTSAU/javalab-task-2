/**
 * 
 */
function clickSelectedElements() {
	var elems = document.getElementsByClassName('search-input-selected');
	for(var i=0; i<elems.length; i++) {
		elems[i].click();
	}
	if(elems.length == 0) {
		$("input[id*='author-button-']")[0].click();
	}
}

function disableDeadLinks() {
	$("li.disabled a").each(function(index, elem) {
		this.href="";
	});
}

window.onload=function() {
	disableDeadLinks();
	clickSelectedElements();
}

function showPopover(id) {
	var str = '<div class="btn-group" role="group"><button  class="btn btn-primary btn-xs" type="submit">' +
		$('#del-'+id).data('textOk') + '</button>' +
		'<button  class="btn btn-default btn-xs" type="button" onclick="$(&quot;#del-' +
		id+'&quot;).popover(&quot;hide&quot;);">' +
		$('#del-'+id).data('textCancel') + '</button></div>';
	
	$('#del-'+id).popover({animation:true, content:str, html:true});
}

$(function () {
	$('[data-toggle="popover"]').click();
});
