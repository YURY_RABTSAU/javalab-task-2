/**
 * 
 */
function setLocale(locale) {
	var url = location.href;
	
	if (url.indexOf('lang=') > -1) {
		url = url.replace(/lang=../, 'lang=' + locale);
		
	} else {
		if (url.indexOf('?') > -1) {
			url = url + '&lang=' + locale;
		} else {
			url = url + '?lang=' + locale;
		}
	}
	location.href = url;
	
	location.reload("true");
	var link = document.getElementById('locale-link');
	link.setAttribute('href', url);
	link.click();
}
