/**
 * 
 */
var justValidate = false; //flag that indicates is validate or submit

function Submit(){
	if(justValidate){
		justValidate = false;
		return false;
	}
}

function validate(button){
	justValidate = true;
	var form = $(button).data('form');
	var modal = $(button).data('modal');
	if($(form)[0].checkValidity()){
		$(modal).modal('show');
	}else{
		$(modal).modal('hide');
	}
}
