<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<body>

	<div id="menu" class="list-group">

		<a class="list-group-item" href="./home">
			<spring:message code="menu.home" />
		</a> 
		<a class="list-group-item" href="./news"> 
			<spring:message	code="menu.back" />
		</a> 
		<a class="list-group-item" href="./news_new"> 
			<spring:message	code="menu.addNews" />
		</a> 
		<a class="list-group-item" href="./authors">
			<spring:message	code="menu.authors" />
		</a>
		<a class="list-group-item" href="./tags">
			<spring:message	code="menu.tags" />
		</a>

	</div>

</body>
</html>