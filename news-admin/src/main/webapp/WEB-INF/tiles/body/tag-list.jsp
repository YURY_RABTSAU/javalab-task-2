<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="./resources/js/add-edit.js" ></script>
<script type="text/javascript" src="./resources/js/validation.js" ></script>
<title><spring:message code="title.tagsEdit" /></title>
</head>
<body>

	<ul class="list-droup" >
		<c:forEach var="tag" items="${allTags}">
			<li class="list-group-item">
				<div class="static-item static-${tag.id}">
					<c:out value="${tag.name}"></c:out>
					<button class="btn btn-default" onclick="editShow(${tag.id})">
						<span class="glyphicon glyphicon-edit"></span> |
						<spring:message code="edit.edit" /></button>
				</div>
				<div class="dynamic-item dynamic-${tag.id}">
					<form action="./tag_upd" method="post" id="form-${tag.id}" onsubmit="return Submit()">
						<input type="text" value="${tag.name}" name="tag-name"
							required maxlength="30" size="35"
							placeholder="<spring:message code="edit.enterTag" />"
						>
						<input type="hidden" value="${tag.id}" name="tag-id">
						<button class="btn btn-warning" data-form="#form-${tag.id}"
							data-modal="#update-confirm-${tag.id}"
							type="submit" onclick="validate(this)">
							<span class="glyphicon glyphicon-refresh"></span> |
							<spring:message code="edit.update" />
						</button>
						
						<div class="modal fade" id="update-confirm-${tag.id}" >
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body">
										<spring:message code="edit.confirm.tagUpdate" />
									</div>
									<div class="modal-footer">
										<button class="btn btn-primary" 
											type="submit">
											<spring:message code="edit.confirm.OK" />
										</button>
										<button class="btn btn-default"
											type="button" data-dismiss="modal">
											<spring:message code="edit.confirm.cancel" />
										</button>
									</div>
								</div>
							</div>
						</div>
						
					</form>
					
					<form action="./tag_del" method="post">
						<input type="hidden" value="${tag.id}" name="tag-id">
						<button class="btn btn-danger" data-toggle="modal"
							type="button" data-target="#delete-confirm-${tag.id}">
							<span class="glyphicon glyphicon-remove"></span> |
							<spring:message code="edit.delete" />
						</button>
					
						<div class="modal fade" id="delete-confirm-${tag.id}" >
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body">
										<spring:message code="edit.confirm.tagDelete" />
									</div>
									<div class="modal-footer">
										<button class="btn btn-primary" 
											type="submit">
											<spring:message code="edit.confirm.OK" />
										</button>
										<button class="btn btn-default"
											type="button" data-dismiss="modal">
											<spring:message code="edit.confirm.cancel" />
										</button>
									</div>
								</div>
							</div>
						</div>	
					
					</form>
					<button class="btn btn-default" onclick="editHide(${tag.id})">
						<spring:message code="edit.cancel" />
					</button>
				</div>
			</li>
		</c:forEach>
		
		<li class="list-group-item">
			<div class="static-item static-0">
				<button class="btn btn-primary" onclick="editShow(0)">
					<span class="glyphicon glyphicon-plus"></span> |
					<spring:message code="edit.addTag" /></button>
			</div>
			<div class="dynamic-item dynamic-0">
				<form action="./tag_new" method="post" id="form-0" onsubmit="return Submit()">
					<input type="text" value="" name="tag-name" 
						required maxlength="30" size="35"
						placeholder="<spring:message code="edit.enterTag" />"
					>
					<button class="btn btn-success" data-modal="#new-confirm"
							data-form="#form-0"
							type="submit" onclick="validate(this)">
						<spring:message code="edit.add" />
					</button>
					
					<div class="modal fade" id="new-confirm" >
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body">
										<spring:message code="edit.confirm.tagNew" />
									</div>
									<div class="modal-footer">
										<button class="btn btn-primary" 
											type="submit">
											<spring:message code="edit.confirm.OK" />
										</button>
										<button class="btn btn-default"
											type="button" data-dismiss="modal">
											<spring:message code="edit.confirm.cancel" />
										</button>
									</div>
								</div>
							</div>
						</div>
					
				</form>
				<button class="btn btn-warning" onclick="editHide(0)">
					<spring:message code="edit.cancel" />
				</button>
			</div>
		</li>
	</ul>

</body>
</html>