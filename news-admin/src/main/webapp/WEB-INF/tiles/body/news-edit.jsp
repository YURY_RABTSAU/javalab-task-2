<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="./resources/js/news.js" ></script>
<script type="text/javascript" src="./resources/js/validation.js" ></script>
<title><spring:message code="title.newsEdit" /></title>
</head>
<body>

	<form action="" method="post" id="news-form" onsubmit="return Submit()">
		<input type="hidden" name="news-id" value="${newsVO.id}">
		
		<label><spring:message code="editNews.titleLabel" />:</label>
		<input type="text" name="news-title" value="${newsVO.title}" id="input-title"
			placeholder="<spring:message code="editNews.titlePlaceholder" />" 
			maxlength="30" width="60" required="required">
		<br>
		
		<spring:message code="body.datePattern" var="datePattern" />
		<label><spring:message code="editNews.dateLabel" />:</label>
		<input type="text" name="news-date" 
			value="<fmt:formatDate pattern="${datePattern}" 
				value="${newsVO.creationDate}" />" disabled="disabled">
		<br>
		
		<label><spring:message code="editNews.shortTextLabel" />:</label><br>
		<textarea name="news-short" id="short-input"
			placeholder="<spring:message code="editNews.shortTextPlaceholder" />"
			maxlength="100" required="required">${newsVO.shortText}</textarea>
		<br>
		
		<label><spring:message code="editNews.fullTextLabel" />:</label><br>
		<textarea name="news-full" id="full-input"
			placeholder="<spring:message code="editNews.fullTextPlaceholder" />" 
			maxlength="2000" required="required">${newsVO.fullText}</textarea>
		<br>
		
		<div id="author-box" class="btn-group">
			<button data-toggle="dropdown"
				class="btn btn-default dropdown-toggle"
				data-placeholder="Please select">
				<spring:message code="search.selectAuthor" />
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu pull-top">
				<c:forEach var="author" items="${allAuthors}">
					<c:if test="${author.expired == null}">
					<li><input type="radio" id="author-button-${author.id}"
						name="select-author" value="${author.id}"
						<c:if test="${newsVO.author.id == author.id}">
							class="search-input-selected"</c:if> 
						> 
						<label for="author-button-${author.id}"> 
							<c:out value="${author.name}" />
						</label>
					</li>
					</c:if>
				</c:forEach>
			</ul>

		</div>
		<!-- /author-box -->

		<div id="tag-box" class="btn-group">

			<button data-toggle="dropdown"
				class="btn btn-default dropdown-toggle" data-label-placement>
				<spring:message code="search.selectTags" />
				<span class="caret"></span>
			</button>

			<ul class="dropdown-menu pull-top">
				<c:forEach var="tag" items="${allTags}">
					<li><input type="checkbox" id="tag-checkbox-${tag.id}"
						name="tag-checkbox" value="${tag.id}" 
						<c:if test="${newsTags.contains(tag.id)}">
							class="search-input-selected"</c:if>
						> 
						<label for="tag-checkbox-${tag.id}">
							<c:out value="${tag.name}" />
						</label>
					</li>
				</c:forEach>
			</ul>
		</div>
		<!-- /tag-box -->
		
		<button type="submit" class="btn btn-success" 
			data-modal="#save-news-confirm" data-form="#news-form" onclick="validate(this)">
			<span class="glyphicon glyphicon-floppy-saved"></span> |
			<spring:message code="editNews.save" />
		</button>
		
		<div class="modal fade" id="save-news-confirm">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<spring:message code="editNews.saveConfirm" />
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" type="submit">
							<spring:message code="editNews.saveConfirmOK" />
						</button>
						<button class="btn btn-default" type="button" data-dismiss="modal">
							<spring:message code="editNews.saveConfirmCancel" />
						</button>
					</div>
				</div>
			</div>
		</div>
		
	</form>

</body>
</html>