<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="./resources/js/add-edit.js" ></script>
<script type="text/javascript" src="./resources/js/validation.js" ></script>
<title><spring:message code="title.authorsEdit" /></title>
</head>
<body>

	<ul class="list-droup" >
		<c:forEach var="author" items="${allAuthors}">
			<li class="list-group-item
				<c:if test="${author.expired != null}">list-group-item-danger</c:if>
				">
				<c:if test="${author.expired != null}">
					<c:out value="${author.name}"></c:out>
					(<spring:message code="edit.isExpired" />)
				</c:if>
				
				<c:if test="${author.expired == null}">
				<div class="static-item static-${author.id}">
					<c:out value="${author.name}"></c:out>
					<button class="btn btn-default" onclick="editShow(${author.id})">
						<span class="glyphicon glyphicon-edit"></span> |
						<spring:message code="edit.edit" />
					</button>
				</div>
				<div class="dynamic-item dynamic-${author.id}">
					<form action="./author_upd" method="post" id="form-${author.id}" onsubmit="return Submit()">
						<input type="text" value="${author.name}" name="author-name"
							required maxlength="30" size="35"
							placeholder="<spring:message code="edit.enterAuthor" />"
						>
						<input type="hidden" value="${author.id}" name="author-id">
						<button class="btn btn-warning" onclick="validate(this)"
							type="submit" data-modal="#update-confirm-${author.id}"
							data-form="#form-${author.id}">
							<span class="glyphicon glyphicon-refresh"></span> |
							<spring:message code="edit.update" />
						</button>
						
						<div class="modal fade" id="update-confirm-${author.id}" >
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body">
										<spring:message code="edit.confirm.authorUpdate" />
									</div>
									<div class="modal-footer">
										<button class="btn btn-primary" 
											type="submit">
											<spring:message code="edit.confirm.OK" />
										</button>
										<button class="btn btn-default"
											type="button" data-dismiss="modal">
											<spring:message code="edit.confirm.cancel" />
										</button>
									</div>
								</div>
							</div>
						</div>
						
					</form>
					
					<form action="./author_exp" method="post">
						<input type="hidden" value="${author.id}" name="author-id">
						<button class="btn btn-danger" data-toggle="modal"
							type="button" data-target="#expire-confirm-${author.id}">
							<span class="glyphicon glyphicon-ban-circle"></span> |
							<spring:message code="edit.expire" />
						</button>
						
						<div class="modal fade" id="expire-confirm-${author.id}" >
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body">
										<spring:message code="edit.confirm.authorExpire" />
									</div>
									<div class="modal-footer">
										<button class="btn btn-primary" 
											type="submit">
											<spring:message code="edit.confirm.OK" />
										</button>
										<button class="btn btn-default"
											type="button" data-dismiss="modal">
											<spring:message code="edit.confirm.cancel" />
										</button>
									</div>
								</div>
							</div>
						</div>
						
					</form>
					<button class="btn btn-default" onclick="editHide(${author.id})"
						><spring:message code="edit.cancel" /></button>
				</div>
				</c:if>
			</li>
		</c:forEach>
		
		<li class="list-group-item">
			<div class="static-item static-0">
				<button class="btn btn-primary" onclick="editShow(0)">
					<span class="glyphicon glyphicon-plus"></span> |
					<spring:message code="edit.addAuthor" /></button>
			</div>
			<div class="dynamic-item dynamic-0">
				<form action="./author_new" method="post" id="form-0" onsubmit="return Submit()">
					<input type="text" value="" name="author-name"
						required maxlength="30" size="35"
						placeholder="<spring:message code="edit.enterAuthor" />"
					>
					<button class="btn btn-success" data-form="#form-0"
							type="submit" data-modal="#new-confirm" onclick="validate(this)">
						<spring:message code="edit.add" />
					</button>
					
					<div class="modal fade" id="new-confirm" >
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body">
										<spring:message code="edit.confirm.authorNew" />
									</div>
									<div class="modal-footer">
										<button class="btn btn-primary" 
											type="submit">
											<spring:message code="edit.confirm.OK" />
										</button>
										<button class="btn btn-default"
											type="button" data-dismiss="modal">
											<spring:message code="edit.confirm.cancel" />
										</button>
									</div>
								</div>
							</div>
						</div>
				
				</form>
				<button class="btn btn-warning" onclick="editHide(0)">
					<spring:message code="edit.cancel" />
				</button>
			</div>
		</li>
	</ul>

</body>
</html>