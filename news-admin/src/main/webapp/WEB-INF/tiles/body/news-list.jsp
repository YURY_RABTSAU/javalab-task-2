<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="./resources/js/news.js" ></script>
<title><spring:message code="title.newsList" /></title>
</head>
<body>

	<!-- search -->
	<spring:message code="search.search" />:
	<div id="search-box" >
		<form action="./search" method="post">
			
			<div id="author-box" class="btn-group">
				<button data-toggle="dropdown"
					class="btn btn-default dropdown-toggle"
					data-placeholder="Please select">
					<spring:message code="search.selectAuthor" />
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<input type="radio" id="author-button-none"
							name="select-author" value="-1"
							<c:if test="${selectedAuthor == null}">
								class="search-input-selected"</c:if> 
						> 
						<label for="author-button-none"> 
							<spring:message code="search.selectAuthor" />
						</label>
					</li>
					<c:forEach var="author" items="${allAuthors}">
						<li><input type="radio" id="author-button-${author.id}"
							name="select-author" value="${author.id}"
							<c:if test="${selectedAuthor == author.id}">
								class="search-input-selected"</c:if> 
							> 
							<label for="author-button-${author.id}"> 
								<c:out value="${author.name}" />
							</label>
						</li>
					</c:forEach>
				</ul>

			</div>
			<!-- /author-box -->

			<div id="tag-box" class="btn-group">

				<button data-toggle="dropdown"
					class="btn btn-default dropdown-toggle" data-label-placement>
					<spring:message code="search.selectTags" />
					<span class="caret"></span>
				</button>

				<ul class="dropdown-menu">
					<c:forEach var="tag" items="${allTags}">
						<li><input type="checkbox" id="tag-checkbox-${tag.id}"
							name="tag-checkbox" value="${tag.id}" 
							<c:if test="${selectedTags.contains(tag.id)}">
								class="search-input-selected"</c:if>
							> 
							<label for="tag-checkbox-${tag.id}">
								<c:out value="${tag.name}" />
							</label>
						</li>
					</c:forEach>
				</ul>
			</div>
			<!-- /tag-box -->

			<div id="submit-box" class="btn-group">
				<button class="btn btn-primary" type="submit" name="act"
					value="setSearch">
					<span class="glyphicon glyphicon-ok"></span> |
					<spring:message code="search.applySearch"></spring:message>
				</button>
				<button class="btn btn-danger" type="submit" name="act" 
					value="dropSearch">
					<span class="glyphicon glyphicon-remove"></span> |
					<spring:message code="search.dropSearch"></spring:message>
				</button>
			</div>
		</form>
	</div><!-- /search -->
	
	<div id="no-result-block">
		<c:if test="${empty newsList}">
			<h3><spring:message code="search.noResults" /></h3>
			<img alt="<spring:message code="search.noResults" />" 
				src="./resources/img/no_result.jpg">
		</c:if>
	</div>
	
	<c:forEach var="news" items="${newsList}">
		<div class="news-preview-box panel panel-default">
		
			<div class="panel-heading">
				<div class="news-title panel-title">${news.title}</div>
				<div class="author">
					(<spring:message code="body.author" /> ${news.author.name})
				</div>
				
				<div class="modification-date">
					<spring:message code="body.modified" />:
					<spring:message code="body.datePattern" var="datePattern" />
					
					<fmt:formatDate pattern="${datePattern}"
						value="${news.modificationDate}" />
				</div>
			</div>
			
			<div class="panel-body">
				<div class="short-text">
					<c:out value="${news.shortText}" />
				</div>
			</div>
			
			<div class="panel-footer">
				<div class="tags">
					<spring:message code="body.tags" />
					<c:forEach var="tag" items="${news.tags}">
						<c:out value="${tag.name}" />,
					</c:forEach>
				</div>
				
				<div class="comments">
					<spring:message code="body.comments" /> 
					(<c:out value="${news.numberOfComments}" />)
				</div>
				
				<a href="./newsdetail?id=${news.id}" >
					<spring:message code="body.read-more" />
				</a>
			</div>
			
		</div>
	</c:forEach>
	
	<!-- pagination -->
	<div>
		<ul class="pagination" >
			<!-- first -->
			<li
				<c:if test="${pageNumber <= 1}">class="disabled"</c:if>
			>
				<a href="?page=1">
					<span class="glyphicon glyphicon-fast-backward"></span>
				</a>
			</li>
			
			<!-- previous -->
			<li
				<c:if test="${pageNumber <= 1}">class="disabled"</c:if>
			>
				<a href="?page=${pageNumber-1}">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
			</li>
			
			<c:forEach var="pageIndex" begin="1" end="${pagesCount}" >
				<li 
					<c:if test="${pageIndex==pageNumber}">class="active"</c:if>
				>
					<a href="?page=${pageIndex}">${pageIndex}</a>
				</li>
			</c:forEach>
			
			<!-- next -->
			<li
				<c:if test="${pageNumber >= pagesCount}">class="disabled"</c:if>
			>
				<a href="?page=${pageNumber+1}">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</li>
			
			<!-- last -->
			<li
				<c:if test="${pageNumber >= pagesCount}">class="disabled"</c:if>
			>
				<a href="?page=${pagesCount}">
					<span class="glyphicon glyphicon-fast-forward"></span>
				</a>
			</li>
		</ul>
	</div><!-- /pagination -->

</body>
</html>