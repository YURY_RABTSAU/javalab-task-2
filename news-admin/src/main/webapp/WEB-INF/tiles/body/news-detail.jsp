<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="./resources/js/news.js" ></script>
<title><spring:message code="title.newsDetail" /></title>
</head>
<body>

	<!-- news -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="news-title panel-title">${newsVO.title}</div>
			<div class="author">
				(<spring:message code="body.author" /> ${newsVO.author.name})
			</div>
			
			<div class="tags">
				<spring:message code="body.tags" />
				<c:forEach var="tag" items="${newsVO.tags}">
					<c:out value=" ${tag.name}" />,
				</c:forEach>
			</div>
			
			<div class="modification-date">
				<spring:message code="body.modified" />:
				<spring:message code="body.datePattern" var="datePattern" />

				<fmt:formatDate pattern="${datePattern}"
					value="${newsVO.modificationDate}" />
			</div>
		</div>
		<!-- /news header -->
		
		<div class="news-full-text panel-body">
			<pre><c:out value="${newsVO.fullText}" /></pre>
		</div>
	</div>
	<!-- /news -->
	
	<!-- prev/next -->
	<div>
		<ul class="pager">
			<li class="previous 
				<c:if test="${prevId == null}">disabled</c:if>
			">
				<a href="?id=${prevId}">
					<spring:message code="body.prevNews" />
				</a>
			</li>
			<li class="next
				<c:if test="${nextId == null}">disabled</c:if>
			">
				<a href="?id=${nextId}">
					<spring:message code="body.nextNews" />
				</a>
			</li>
		</ul>
	</div>
	<!-- /prev/next -->
	
	<div id="admin-form-container">
	
	<form action="./news_upd" method="get" class="admin-form">
		<input type="hidden" name="id" value="${newsVO.id}">
		<button class="btn btn-warning" type="submit">
			<span class="glyphicon glyphicon-edit"></span> |
			<spring:message code="body.editNews" />
		</button>
	</form>

	<form action="./news_del" method="post" class="admin-form">
		<input type="hidden" name="newsId" value="${newsVO.id}">
		<button class="btn btn-danger" type="button" data-toggle="modal"
			data-target="#delete-news-confirm">
			<span class="glyphicon glyphicon-floppy-remove"></span> |
			<spring:message code="body.delNews" />
		</button>

		<div class="modal fade" id="delete-news-confirm">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<spring:message code="body.submitDelNews" />
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" type="submit">
							<spring:message code="body.submitDelNewsOK" />
						</button>
						<button class="btn btn-default" type="button" data-dismiss="modal">
							<spring:message code="body.submitDelNewsCancel" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</form>

	</div>
	<!-- all comments -->
	<div class="panel panel-default col-xs-7">
		<div class="panel-heading">
			<spring:message code="body.comments" />
			:
		</div>
		<div class="panel-body">
			<c:forEach var="comment" items="${newsVO.comments}">
				<!-- single comment -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<spring:message code="body.dateTimePattern" 
							var="dateTimePattern" />
						<fmt:formatDate pattern="${dateTimePattern}"
							value="${comment.creationDate}" />
						
						<div class="delete-comment-form">	
						<form action="./comment_del" method="post">
							<input type="hidden" name="newsId" value="${newsVO.id}">
							<input type="hidden" name="commentId" value="${comment.id}">
							<button id="del-${comment.id}" class="btn btn-danger" 
								type="button" onclick="showPopover(${comment.id})"
								data-toggle="popover" data-placement="top"
								data-title="<spring:message code="body.submitDelComment" />" 
								data-text-ok="<spring:message code="body.submitDelCommentOK" />" 
								data-text-cancel="<spring:message code="body.submitDelCommentCancel" />">
								<span class="glyphicon glyphicon-remove"></span>
							</button>
							
							
						</form>
						</div>
					</div>
					<div class="panel-body">
						<c:out value="${comment.text}" />
					</div>
				</div>
				<!-- /single comment -->
			</c:forEach>
			
			<!-- new comment -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<spring:message code="body.newComment" />
				</div>
				<div class="new-comment-form panel-body">
					<form action="./comment" method="post">
						<input type="hidden" name="newsId" value="${newsVO.id}">
						<textarea name="commentText" required maxlength="100" 
							placeholder="<spring:message code="body.typeHere" />"
							></textarea>
						<button class="btn btn-primary" type="submit">
							<span class="glyphicon glyphicon-ok"></span> |
							<spring:message code="body.submitComment" />
						</button>
					</form>
				</div>
			</div><!-- /new comment -->
			
		</div>
	</div>
	<!-- /all comments -->


</body>
</html>