package com.epam.newscommon.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newscommon.dao.jdbc.DateParseUtils;
import com.epam.newscommon.entity.AuthorTO;
import com.epam.newscommon.entity.CommentTO;
import com.epam.newscommon.entity.NewsTO;
import com.epam.newscommon.entity.NewsVO;
import com.epam.newscommon.entity.SearchCriteria;
import com.epam.newscommon.entity.TagTO;
import com.epam.newscommon.service.impl.NewsManagerImpl;

public class NewsManagerTest {

	private NewsManagerImpl nManager;
	@Mock
	private NewsService nService;
	@Mock
	private AuthorService aService;
	@Mock
	private CommentService cService;
	@Mock
	private TagService tService;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		nManager = new NewsManagerImpl();
		nManager.setNewsService(nService);
		nManager.setAuthorService(aService);
		nManager.setCommentService(cService);
		nManager.setTagService(tService);
	}

	@Test
	public void testGetNewsById() throws Exception {
		NewsTO newsTO = new NewsTO();
		newsTO.setId(5L);
		newsTO.setTitle("title");
		newsTO.setShortText("bla-bla");
		newsTO.setFullText("bla-bla-bla-bla-bla");
		newsTO.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		newsTO.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		when(nService.getNewsById(5L)).thenReturn(newsTO);
		
		AuthorTO author = new AuthorTO();
		author.setId(1L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		when(aService.getAuthorByNews(5L)).thenReturn(author);
		
		List<CommentTO> commentList = new ArrayList<CommentTO>();
		when(cService.getCommentsByNews(5L)).thenReturn(commentList);
		
		List<TagTO> tagList = new ArrayList<TagTO>();
		when(tService.getTagsByNews(5L)).thenReturn(tagList);
		
		NewsVO newsVO = new NewsVO();
		newsVO.setNewsTO(newsTO);
		newsVO.setAuthor(author);
		newsVO.setComments(commentList);
		newsVO.setTags(tagList);

		NewsVO returnedNewsVO = nManager.getNewsById(5L);

		assertNotNull(returnedNewsVO);
		assertEquals(newsVO, returnedNewsVO);
		verify(nService).getNewsById(5L);
		verify(aService).getAuthorByNews(5L);
		verify(cService).getCommentsByNews(5L);
		verify(tService).getTagsByNews(5L);
		verifyNoMoreInteractions(nService, aService, cService, tService);
	}

	@Test
	public void testGetNewsByCriteria() throws Exception {
		long newsId1 = 87L;
		long newsId2 = 84L;
		SearchCriteria criteria = new SearchCriteria();
		criteria.setAuthorId(5L);
		for (int i = 1; i < 7; i++) {
			long tagId = i*2;
			criteria.addTag(tagId);
		}
		NewsTO resNews1 = new NewsTO();
		resNews1.setId(newsId1);
		NewsTO resNews2 = new NewsTO();
		resNews2.setId(newsId2);
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		newsList.add(resNews1);
		newsList.add(resNews2);
		when(nService.getNews(criteria)).thenReturn(newsList);

		nManager.getNews(criteria);

		verify(nService).getNews(criteria);
		verifyNoMoreInteractions(nService);

		verify(aService).getAuthorByNews(newsId1);
		verify(cService).getCommentsByNews(newsId1);
		verify(tService).getTagsByNews(newsId1);

		verify(aService).getAuthorByNews(newsId2);
		verify(cService).getCommentsByNews(newsId2);
		verify(tService).getTagsByNews(newsId2);

		verifyNoMoreInteractions(aService, cService, tService);
	}

	@Test
	public void testAddNews() throws Exception {
		NewsTO newsTO = new NewsTO();
		newsTO.setId(5L);
		newsTO.setTitle("title");
		newsTO.setShortText("bla-bla");
		newsTO.setFullText("bla-bla-bla-bla-bla");
		newsTO.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		newsTO.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		
		AuthorTO author = new AuthorTO();
		author.setId(1L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		
		//List<CommentTO> commentList = new ArrayList<CommentTO>();
		
		List<TagTO> tagList = new ArrayList<TagTO>();
		
		NewsVO newsVO = new NewsVO();
		newsVO.setNewsTO(newsTO);
		newsVO.setAuthor(author);
		//newsVO.setComments(commentList);
		newsVO.setTags(tagList);

		nManager.addNews(newsVO);

		verify(nService).addNews(newsTO);
		verify(aService).connectAuthorAndNews(1L, 5L);
		//verify(cService).changeNewsComments(5L, commentList);
		verify(tService).changeNewsTags(5L, new ArrayList<Long>());
		verifyNoMoreInteractions(nService, aService, cService, tService);
	}

}
