package com.epam.newscommon.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.dao.jdbc.DateParseUtils;
import com.epam.newscommon.entity.NewsTO;
import com.epam.newscommon.entity.SearchCriteria;
import com.epam.newscommon.service.impl.NewsServiceImpl;

public class NewsServiceTest {

	private NewsServiceImpl nService;
	@Mock
	private NewsDao newsDao;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		nService = new NewsServiceImpl();
		nService.setNewsDao(newsDao);
	}

	@Test
	public void testGetNewsById() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		when(newsDao.getNewsById(4L)).thenReturn(news);

		NewsTO returnedNews = nService.getNewsById(4L);

		assertNotNull(returnedNews);
		assertEquals(news, returnedNews);
		verify(newsDao).getNewsById(4L);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testGetNewsByCriteria() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		List<NewsTO> list = new ArrayList<NewsTO>();
		list.add(news);
		SearchCriteria criteria = new SearchCriteria();
		criteria.setAuthorId(3L);
		criteria.addTag(5L);
		when(newsDao.getNews(criteria)).thenReturn(list);
		
		List<NewsTO> returnedList = nService.getNews(criteria);
		
		assertNotNull(returnedList);
		assertEquals(1, returnedList.size());
		assertEquals(news, returnedList.get(0));
		verify(newsDao).getNews(criteria);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testCreateNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		when(newsDao.createNews(news)).thenReturn(1042L);
		
		long returnedId = nService.createNews(news);
		
		assertEquals(1042L, returnedId);
		verify(newsDao).createNews(news);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testUpdateNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		
		nService.updateNews(news);
		
		verify(newsDao).updateNews(news);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testAddNewNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		when(newsDao.getNewsById(4L)).thenReturn(null);
		
		nService.addNews(news);
		
		verify(newsDao).getNewsById(4L);
		verify(newsDao).createNews(news);
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testAddExistingNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		when(newsDao.getNewsById(4L)).thenReturn(new NewsTO());
		
		nService.addNews(news);
		
		verify(newsDao).getNewsById(4L);
		verify(newsDao).updateNews(news);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testDeleteNews() throws Exception {
		nService.deleteNews(16L);
		
		verify(newsDao).deleteNews(16L);
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testGetNextNews() throws Exception {
		SearchCriteria criteria = new SearchCriteria();
		when(newsDao.getNextNews(criteria, 5L)).thenReturn(3L);
		
		long returnedValue = nService.getNextNews(criteria, 5L);
		
		assertEquals(3L, returnedValue);
	}
	
	@Test
	public void testGetPreviousNews() throws Exception {
		SearchCriteria criteria = new SearchCriteria();
		when(newsDao.getPreviousNews(criteria, 4L)).thenReturn(13L);
		
		long returnedValue = nService.getPreviousNews(criteria, 4L);
		
		assertEquals(13L, returnedValue);
	}

}
