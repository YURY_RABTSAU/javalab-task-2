package com.epam.newscommon.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newscommon.dao.CommentDao;
import com.epam.newscommon.dao.jdbc.DateParseUtils;
import com.epam.newscommon.entity.CommentTO;
import com.epam.newscommon.service.impl.CommentServiceImpl;

public class CommentServiceTest {

	private CommentServiceImpl cService;
	@Mock
	private CommentDao commentDao;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		cService = new CommentServiceImpl();
		cService.setCommentDao(commentDao);
	}

	@Test
	public void testGetCommentById() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setId(5L);
		comment.setNewsId(12L);
		comment.setText("text");
		comment.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2014-09-30 17:25:03.007"));
		when(commentDao.getCommentById(5L)).thenReturn(comment);
		
		CommentTO returnedComment = cService.getCommentById(5L);
		
		assertNotNull(returnedComment);
		assertEquals(comment, returnedComment);
		verify(commentDao).getCommentById(5L);
		verifyNoMoreInteractions(commentDao);
	}

	@Test
	public void testGetCommentsByNews() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setId(5L);
		comment.setNewsId(12L);
		comment.setText("text");
		comment.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2014-09-30 17:25:03.007"));
		List<CommentTO> commentList = new ArrayList<CommentTO>();
		commentList.add(comment);
		when(commentDao.getCommentsByNews(12L)).thenReturn(commentList);
		
		List<CommentTO> returnedList = cService.getCommentsByNews(12L);
		
		assertNotNull(returnedList);
		assertEquals(1, returnedList.size());
		assertEquals(comment, returnedList.get(0));
		verify(commentDao).getCommentsByNews(12L);
		verifyNoMoreInteractions(commentDao);
	}

	@Test
	public void testCreateComment() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setId(5L);
		comment.setNewsId(12L);
		comment.setText("text");
		comment.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2014-09-30 17:25:03.007"));
		when(commentDao.createComment(comment)).thenReturn(1012L);
		
		long returnedId = cService.createComment(comment);
		
		assertEquals(1012L, returnedId);
		verify(commentDao).createComment(comment);
		verifyNoMoreInteractions(commentDao);
	}

	@Test
	public void testUpdateComment() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setId(5L);
		comment.setNewsId(12L);
		comment.setText("text");
		comment.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2014-09-30 17:25:03.007"));
		
		cService.updateComment(comment);
		
		verify(commentDao).updateComment(comment);
		verifyNoMoreInteractions(commentDao);
	}

	@Test
	public void testAddNewComment() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setId(5L);
		comment.setNewsId(12L);
		comment.setText("text");
		comment.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2014-09-30 17:25:03.007"));
		when(commentDao.getCommentById(5L)).thenReturn(null);
		
		cService.addComment(comment);
		
		verify(commentDao).getCommentById(5L);
		verify(commentDao).createComment(comment);
		verifyNoMoreInteractions(commentDao);
	}
	
	@Test
	public void testAddExistingComment() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setId(5L);
		comment.setNewsId(12L);
		comment.setText("text");
		comment.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2014-09-30 17:25:03.007"));
		when(commentDao.getCommentById(5L)).thenReturn(new CommentTO());
		
		cService.addComment(comment);
		
		verify(commentDao).getCommentById(5L);
		verify(commentDao).updateComment(comment);
		verifyNoMoreInteractions(commentDao);
	}

	@Test
	public void testDeleteComment() throws Exception {
		cService.deleteComment(7L);
		
		verify(commentDao).deleteComment(7L);
		verifyNoMoreInteractions(commentDao);
	}

	@Test
	public void testAddCommentsToNews() throws Exception {
		List<CommentTO> commentsList = new ArrayList<CommentTO>();
		CommentTO comment1 = new CommentTO();
		CommentTO comment2 = new CommentTO();
		CommentTO comment3 = new CommentTO();
		comment1.setId(31L);
		comment1.setNewsId(11L);
		comment2.setId(32L);
		comment2.setNewsId(11L);
		comment3.setId(33L);
		comment3.setNewsId(11L);
		commentsList.add(comment1);
		commentsList.add(comment2);
		commentsList.add(comment3);
		when(commentDao.getCommentById(anyLong())).thenReturn(null);
		
		cService.addCommentsToNews(11L, commentsList);
		
		verify(commentDao).createComment(comment1);
		verify(commentDao).createComment(comment2);
		verify(commentDao).createComment(comment3);
		verify(commentDao).getCommentById(31L);
		verify(commentDao).getCommentById(32L);
		verify(commentDao).getCommentById(33L);
		verifyNoMoreInteractions(commentDao);
	}

	@Test
	public void testChangeNewsComments() throws Exception {
		List<CommentTO> commentsList = new ArrayList<CommentTO>();
		List<CommentTO> oldCommentsList = new ArrayList<CommentTO>();
		CommentTO comment1 = new CommentTO();
		CommentTO comment2 = new CommentTO();
		comment1.setId(31L);
		comment1.setNewsId(11L);
		comment2.setId(32L);
		comment2.setNewsId(11L);
		oldCommentsList.add(comment1);
		oldCommentsList.add(comment2);
		when(commentDao.getCommentsByNews(11L)).thenReturn(oldCommentsList);
		
		cService.changeNewsComments(11L, commentsList);
		
		verify(commentDao).getCommentsByNews(11L);
		verify(commentDao).deleteComment(31L);
		verify(commentDao).deleteComment(32L);
		verifyNoMoreInteractions(commentDao);
	}

}
