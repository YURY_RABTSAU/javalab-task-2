package com.epam.newscommon.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.entity.NewsTO;
import com.epam.newscommon.entity.SearchCriteria;
import com.epam.newscommon.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:BeansForTesting.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:NewsDaoTestData.xml")
public class JdbcNewsDaoTest {

	@Autowired
	private NewsDao newsDao;

	@Test
	public void testGetNewsById() throws DaoException {
		NewsTO news = newsDao.getNewsById(7L);

		assertNotNull(news);
		assertEquals(7L, news.getId());
		assertEquals("title7", news.getTitle());
		assertEquals("short text 7", news.getShortText());
		assertEquals("full text 7", news.getFullText());
		assertEquals(
				DateParseUtils
						.getTimestampDateFromString("2015-02-23 17:15:00.000"),
				news.getCreationDate());
		assertEquals(DateParseUtils.getSimpleDateFromString("2015-03-08"),
				news.getModificationDate());
	}

	@Test
	public void testGetNewsByAuthor() throws DaoException {
		SearchCriteria criteria = new SearchCriteria();
		criteria.setAuthorId(9L);
		List<NewsTO> list = newsDao.getNews(criteria); // expected 12,5,4,8

		assertNotNull(list);
		assertEquals(4, list.size());

		NewsTO news0 = list.get(0);
		assertEquals(12L, news0.getId());
		assertEquals("title12", news0.getTitle());
		assertEquals("short text 12", news0.getShortText());
		assertEquals("full text 12", news0.getFullText());
		assertEquals(
				DateParseUtils
						.getTimestampDateFromString("2015-02-23 17:15:00.000"),
				news0.getCreationDate());
		assertEquals(DateParseUtils.getSimpleDateFromString("2015-03-08"),
				news0.getModificationDate());

		assertEquals(5L, list.get(1).getId());
		assertEquals(4L, list.get(2).getId());
		assertEquals(8L, list.get(3).getId());
	}

	@Test
	public void testGetNewsByTag() throws DaoException {
		SearchCriteria criteria = new SearchCriteria();
		criteria.addTag(1L);

		List<NewsTO> list = newsDao.getNews(criteria); // expected 1,12,5,4,2
		assertNotNull(list);
		assertEquals(5, list.size());

		NewsTO news1 = list.get(1);
		assertEquals(12L, news1.getId());
		assertEquals("title12", news1.getTitle());
		assertEquals("short text 12", news1.getShortText());
		assertEquals("full text 12", news1.getFullText());
		assertEquals(
				DateParseUtils
						.getTimestampDateFromString("2015-02-23 17:15:00.000"),
				news1.getCreationDate());
		assertEquals(DateParseUtils.getSimpleDateFromString("2015-03-08"),
				news1.getModificationDate());

		assertEquals(1L, list.get(0).getId());
		assertEquals(5L, list.get(2).getId());
		assertEquals(4L, list.get(3).getId());
		assertEquals(2L, list.get(4).getId());
	}

	@Test
	public void testGetNewsByComplexCriteria() throws DaoException {
		SearchCriteria criteria = new SearchCriteria();
		criteria.addTag(11L);//11
		criteria.setAuthorId(7L);//14

		List<NewsTO> list = newsDao.getNews(criteria); // expected 14,11

		assertNotNull(list);
		assertEquals(2, list.size());

		assertEquals(14L, list.get(0).getId());
		assertEquals(11L, list.get(1).getId());
	}

	@Test
	public void testCreateNews() throws DaoException {
		NewsTO news = new NewsTO();
		news.setId(32L);
		news.setTitle("ins title");
		news.setShortText("ins sh text");
		news.setFullText("ins fuuuuuullllll text");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2010-09-06 22:30:15.049"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2013-04-04"));

		long id = newsDao.createNews(news);

		NewsTO insertedNews = newsDao.getNewsById(id);
		assertNotNull(insertedNews);
		assertEquals(id, insertedNews.getId());
		assertEquals("ins title", insertedNews.getTitle());
		assertEquals("ins sh text", insertedNews.getShortText());
		assertEquals("ins fuuuuuullllll text", insertedNews.getFullText());
		assertEquals(
				DateParseUtils
						.getTimestampDateFromString("2010-09-06 22:30:15.049"),
				insertedNews.getCreationDate());
		assertEquals(DateParseUtils.getSimpleDateFromString("2013-04-04"),
				insertedNews.getModificationDate());
	}

	@Test
	public void testUpdateNews() throws DaoException {
		NewsTO news = new NewsTO();
		news.setId(10L);
		news.setTitle("upd title");
		news.setShortText("upd sh text");
		news.setFullText("upd fuuuuuullllll text");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2011-12-06 21:30:14.058"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2013-07-18"));

		newsDao.updateNews(news);

		NewsTO updatedNews = newsDao.getNewsById(10L);
		assertNotNull(updatedNews);
		assertEquals(10L, updatedNews.getId());
		assertEquals("upd title", updatedNews.getTitle());
		assertEquals("upd sh text", updatedNews.getShortText());
		assertEquals("upd fuuuuuullllll text", updatedNews.getFullText());
		assertEquals(
				DateParseUtils
						.getTimestampDateFromString("2011-12-06 21:30:14.058"),
				updatedNews.getCreationDate());
		assertEquals(DateParseUtils.getSimpleDateFromString("2013-07-18"),
				updatedNews.getModificationDate());
	}

	@Test
	public void testDeleteNews() throws DaoException {
		newsDao.deleteNews(12L);

		NewsTO deletedNews = newsDao.getNewsById(12L);
		assertNull(deletedNews);
	}
	
	@Test
	public void testGetNextNews() throws Exception {
		SearchCriteria criteria = new SearchCriteria();
		
		Long nextId = newsDao.getNextNews(criteria, 5L);
		
		assertNotNull(nextId);
		assertEquals(4L, (long)nextId);
	}
	
	@Test
	public void testGetPreviousNews() throws Exception {
		SearchCriteria criteria = new SearchCriteria();
		
		Long nextId = newsDao.getPreviousNews(criteria, 5L);
		
		assertNotNull(nextId);
		assertEquals(12L, (long)nextId);
	}
	
	@Test
	public void testGetNextNewsNotFound() throws Exception {
		SearchCriteria criteria = new SearchCriteria();
		
		List<NewsTO> list = newsDao.getNews(criteria);
		long lastNewsId = list.get(list.size() - 1).getId();
		Long nextId = newsDao.getNextNews(criteria, lastNewsId);
		
		assertNull(nextId);
	}
	
	@Test
	public void testGetPreviousNewsNotFound() throws Exception {
		SearchCriteria criteria = new SearchCriteria();
		
		Long nextId = newsDao.getPreviousNews(criteria, 1L);
		
		assertNull(nextId);
	}

}
