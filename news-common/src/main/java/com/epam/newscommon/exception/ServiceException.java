package com.epam.newscommon.exception;

public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ServiceException() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public ServiceException(String s) {
		// TODO Auto-generated constructor stub
		super(s);
	}
	
	public ServiceException(Throwable t) {
		// TODO Auto-generated constructor stub
		super(t);
	}
	
	public ServiceException(String s, Throwable t) {
		// TODO Auto-generated constructor stub
		super(s, t);
	}

}
