package com.epam.newscommon.service;

import java.util.List;

import com.epam.newscommon.entity.AuthorTO;
import com.epam.newscommon.entity.NewsVO;
import com.epam.newscommon.entity.SearchCriteria;
import com.epam.newscommon.entity.TagTO;
import com.epam.newscommon.exception.ServiceException;

/**
 * Provides high-level service for News
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public interface NewsManager {

	/**
	 * Gets News by its id
	 * 
	 * @param id - id of the news
	 * @return found News or null if nothing was found
	 * @throws ServiceException
	 */
	public NewsVO getNewsById(long id) throws ServiceException;
	
	/**
	 * Gets all news according to given conditions
	 * 
	 * @param tagIds - tags' ids
	 * @param authorId - author's id
	 * @param offset - how many peaces of news to skip
	 * @param limit - maximum number of peaces of news to get
	 * @return List containing all News sorted by most commented news
	 * @throws ServiceException
	 */
	@Deprecated
	public List<NewsVO> getNews(List<Long> tagIds, long authorId,
			int offset, int limit) throws ServiceException;
	
	/**
	 * Finds all news according to given conditions
	 * 
	 * @param searchCriteria - contains conditions of the search
	 * @return List containing all news (empty List if nothing found)
	 * @throws ServiceException
	 */
	public List<NewsVO> getNews(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Gets news written by the certain author
	 * 
	 * @param author - author for search
	 * @return List containing all found News sorted by most commented news
	 * @throws ServiceException
	 */
	@Deprecated
	public List<NewsVO> getNewsByAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Gets news with the certain tag
	 * 
	 * @param tag - tag for search
	 * @return List containing all found News sorted by most commented news
	 * @throws ServiceException
	 */
	@Deprecated
	public List<NewsVO> getNewsByTag(TagTO tag) throws ServiceException;
	
	/**
	 * Gets news which have all tags of the list
	 * 
	 * @param tags - tags for search
	 * @return List containing all found News sorted by most commented news
	 * @throws ServiceException
	 */
	@Deprecated
	public List<NewsVO> getNewsByTags(List<TagTO> tags) throws ServiceException;
	
	/**
	 * Creates new peace of news or edits existing one
	 * 
	 * @param news - peace of news for creation/editing
	 * @throws ServiceException
	 */
	public void addNews(NewsVO news) throws ServiceException;
	
}
