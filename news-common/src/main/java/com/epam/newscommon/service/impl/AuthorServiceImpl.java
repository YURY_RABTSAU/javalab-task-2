package com.epam.newscommon.service.impl;

import java.util.List;

import com.epam.newscommon.dao.AuthorDao;
import com.epam.newscommon.entity.AuthorTO;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.AuthorService;

/**
 * AuthorService implementation
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public class AuthorServiceImpl implements AuthorService {

	private AuthorDao authorDao;

	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	@Override
	public AuthorTO getAuthorById(long id) throws ServiceException {
		try {
			return authorDao.getAuthorById(id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public AuthorTO getAuthorByName(String name) throws ServiceException {
		try {
			return authorDao.getAuthorByName(name);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public AuthorTO getAuthorByNews(long newsId) throws ServiceException {
		try {
			return authorDao.getAuthorByNews(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<AuthorTO> getAllAuthors() throws ServiceException {
		try {
			return authorDao.getAllAuthors();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public long createAuthor(AuthorTO author) throws ServiceException {
		try {
			return authorDao.createAuthor(author);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateAuthor(AuthorTO author) throws ServiceException {
		try {
			authorDao.updateAuthor(author);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addAuthor(AuthorTO author) throws ServiceException {
		try {
			AuthorTO authorCheck = authorDao.getAuthorById(author.getId());
			if (authorCheck == null) {
				authorDao.createAuthor(author);
			} else {
				authorDao.updateAuthor(author);
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteAuthor(long authorId) throws ServiceException {
		try {
			authorDao.deleteAuthor(authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void createNewsAuthorRecord(long newsId, long authorId) throws ServiceException {
		try {
			authorDao.createNewsAuthor(newsId, authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteNewsAuthorRecord(long newsId, long authorId) throws ServiceException {
		try {
			authorDao.deleteNewsAuthor(newsId, authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void connectAuthorAndNews(long authorId, long newsId)
			throws ServiceException {
		
		AuthorTO currentAuthor = getAuthorByNews(newsId);
		if (currentAuthor != null) {
			deleteNewsAuthorRecord(newsId, currentAuthor.getId());
		}
		createNewsAuthorRecord(newsId, authorId);
	}

}
