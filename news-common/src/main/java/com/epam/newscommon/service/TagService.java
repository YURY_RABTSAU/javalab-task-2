package com.epam.newscommon.service;

import java.util.List;

import com.epam.newscommon.dao.TagDao;
import com.epam.newscommon.entity.TagTO;
import com.epam.newscommon.exception.ServiceException;

/**
 * Provides low-level service for TagTO
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public interface TagService {

	/**
	 * Gets tag by its id
	 * @param id - tag's id
	 * @return TagTO containing found tag or null if nothing found
	 * @throws ServiceException
	 * @see {@link TagDao#getTagById(long)}
	 */
	public TagTO getTagById(long id) throws ServiceException;
	
	/**
	 * Gets tag by its name
	 * @param name - tag's name
	 * @return TagTO containing found tag or null if nothing found
	 * @throws ServiceException
	 * @see {@link TagDao#getTagByName(String)}
	 */
	public TagTO getTagByName(String name) throws ServiceException;
	
	/**
	 * Gets tags for the certain news
	 * @param newsId - id of the news
	 * @return List containing all found tags (empty List if nothing found)
	 * @throws ServiceException
	 * @see {@link TagDao#getTagsByNews(long)}
	 */
	public List<TagTO> getTagsByNews(long newsId) throws ServiceException;
	
	/**
	 * Finds all tags
	 * @return List containing all tags
	 * @throws ServiceException
	 * @see {@link TagDao#getAllTags()}
	 */
	public List<TagTO> getAllTags() throws ServiceException;
	
	/**
	 * Creates new tag in the database
	 * @param tag - new tag
	 * @return new tag's id
	 * @throws ServiceException
	 * @see {@link TagDao#createTag(TagTO)}
	 */
	public long createTag(TagTO tag) throws ServiceException;
	
	/**
	 * Updates tag in the database
	 * @param tag - updating tag
	 * @throws ServiceException
	 * @see {@link TagDao#updateTag(TagTO)}
	 */
	public void updateTag(TagTO tag) throws ServiceException;
	
	/**
	 * Creates new tag or updates existing one
	 * @param tag - tag for creation/updating
	 * @throws ServiceException
	 * @see {@link TagDao#createTag(TagTO)}
	 * @see {@link TagDao#updateTag(TagTO)}
	 */
	public void addTag(TagTO tag) throws ServiceException; 
	
	/**
	 * Deletes tag from database
	 * @param tagId - id of the deleting tag
	 * @throws ServiceException
	 * @see {@link TagDao#deleteTag(long)}
	 */
	public void deleteTag(long tagId) throws ServiceException;
	
	/**
	 * Connects news and tag
	 * @param newsId - id of the news
	 * @param tagId - id of the tag
	 * @throws ServiceException
	 * @see {@link TagDao#createNewsTag(long, long)}
	 */
	public void createNewsTagRecord(long newsId, long tagId) throws ServiceException;
	
	/**
	 * Disconnects news and tag
	 * @param newsId - id of the news
	 * @param tagId - id of the tag
	 * @throws ServiceException
	 * @see {@link TagDao#deleteNewsTag(long, long)}
	 */
	public void deleteNewsTagRecord(long newsId, long tagId) throws ServiceException;
	
	/**
	 * Adds tags to the news
	 * 
	 * @param newsId - id of the news where tags are added to
	 * @param tagIds - list of adding tags' ids
	 * @throws ServiceException
	 */
	public void addTagsToNews(Long newsId, List<Long> tagIds) throws ServiceException;

	/**
	 * Replaces old tags of the news with new ones
	 * 
	 * @param newsId - id of the news for processing
	 * @param tagIds - new tags' ids
	 * @throws ServiceException
	 */
	public void changeNewsTags(Long newsId, List<Long> tagIds) throws ServiceException;
	
}
