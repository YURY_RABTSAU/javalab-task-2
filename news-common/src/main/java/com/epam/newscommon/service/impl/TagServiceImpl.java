package com.epam.newscommon.service.impl;

import java.util.List;

import com.epam.newscommon.dao.TagDao;
import com.epam.newscommon.entity.TagTO;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.TagService;

/**
 * TagService implementation
 * 
 * @author Yury_Rabtsau
 * @version 1.6
 * @since 2015-05-07
 *
 */
public class TagServiceImpl implements TagService {

	private TagDao tagDao;
	
	public TagServiceImpl() {}

	public TagServiceImpl(TagDao tagDao) {
		this.setTagDao(tagDao);
	}

	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}

	@Override
	public TagTO getTagById(long id) throws ServiceException {
		try {
			return tagDao.getTagById(id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public TagTO getTagByName(String name) throws ServiceException {
		try {
			return tagDao.getTagByName(name);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<TagTO> getTagsByNews(long newsId) throws ServiceException {
		try {
			return tagDao.getTagsByNews(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<TagTO> getAllTags() throws ServiceException {
		try {
			return tagDao.getAllTags();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public long createTag(TagTO tag) throws ServiceException {
		try {
			return tagDao.createTag(tag);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateTag(TagTO tag) throws ServiceException {
		try {
			tagDao.updateTag(tag);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addTag(TagTO tag) throws ServiceException {
		try {
			TagTO tagCheck = tagDao.getTagById(tag.getId());
			if (tagCheck == null) {
				tagDao.createTag(tag);
			} else {
				tagDao.updateTag(tag);
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteTag(long tagId) throws ServiceException {
		try {
			tagDao.deleteTag(tagId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void createNewsTagRecord(long newsId, long tagId) throws ServiceException {
		try {
			tagDao.createNewsTag(newsId, tagId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteNewsTagRecord(long newsId, long tagId) throws ServiceException {
		try {
			tagDao.deleteNewsTag(newsId, tagId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void addTagsToNews(Long newsId, List<Long> tagIds)
			throws ServiceException {
		if(newsId == null || tagIds == null) {
			return;
		}
		
		for (Long tagId : tagIds) {
			createNewsTagRecord(newsId, tagId);
		}
	}

	@Override
	public void changeNewsTags(Long newsId, List<Long> tagIds)
			throws ServiceException {
		if(newsId == null) {
			return;
		}
		
		List<TagTO> currentTags = getTagsByNews(newsId);
		for (TagTO tagTO : currentTags) {
			deleteNewsTagRecord(newsId, tagTO.getId());
		}
		addTagsToNews(newsId, tagIds);
	}

}
