package com.epam.newscommon.service.impl;

import java.util.Arrays;
import java.util.List;

import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.entity.NewsTO;
import com.epam.newscommon.entity.SearchCriteria;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.NewsService;

/**
 * NewsService implementation
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public class NewsServiceImpl implements NewsService {

	private NewsDao newsDao;
	
	public NewsServiceImpl() {}
	
	public NewsServiceImpl(NewsDao newsDao) {
		this.setNewsDao(newsDao);
	}

	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	@Override
	public NewsTO getNewsById(long id) throws ServiceException {
		try {
			return newsDao.getNewsById(id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	@Deprecated
	public List<NewsTO> getNewsByAuthor(long authorId) throws ServiceException {
		try {
			return newsDao.getNewsByAuthor(authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	@Deprecated
	public List<NewsTO> getNewsByTags(List<Long> tagIds) throws ServiceException {
		try {
			return newsDao.getNewsByTags(tagIds);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	@Deprecated
	public List<NewsTO> getNewsByTags(Long... tagIds) throws ServiceException {
		List<Long> idList = Arrays.asList(tagIds);
		return getNewsByTags(idList);
	}

	@Override
	@Deprecated
	public List<NewsTO> getNews(List<Long> tagIds, long authorId,
			int offset, int limit) throws ServiceException {
		try {
			return newsDao.getNews(tagIds, authorId, offset, limit);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<NewsTO> getNews(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDao.getNews(searchCriteria);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public Long getNextNews(SearchCriteria searchCriteria, 
			long newsId) throws ServiceException {
		try {
			return newsDao.getNextNews(searchCriteria, newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	};
	
	@Override
	public Long getPreviousNews(SearchCriteria searchCriteria, 
			long newsId) throws ServiceException {
		try {
			return newsDao.getPreviousNews(searchCriteria, newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	};

	@Override
	public long createNews(NewsTO news) throws ServiceException {
		try {
			return newsDao.createNews(news);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateNews(NewsTO news) throws ServiceException {
		try {
			newsDao.updateNews(news);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addNews(NewsTO news) throws ServiceException {
		try {
			NewsTO newsCheck = newsDao.getNewsById(news.getId());
			if (newsCheck == null) {
				newsDao.createNews(news);
			} else {
				newsDao.updateNews(news);
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteNews(long newsId) throws ServiceException {
		try {
			newsDao.deleteNews(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
