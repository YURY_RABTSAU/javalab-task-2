package com.epam.newscommon.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class contains full info about news (including author, comments, tags)
 * @author Yury_Rabtsau
 *
 */
public class NewsVO {
	
	private NewsTO news;
	private AuthorTO author;
	private List<CommentTO> comments;
	private List<TagTO> tags;
	
	public NewsVO() {
		news = new NewsTO();
	}
	
	public long getId() {
		return news.getId();
	}
	
	public void setId(long id) {
		news.setId(id);
	}
	
	public String getTitle() {
		return news.getTitle();
	}
	
	public void setTitle(String title) {
		news.setTitle(title);
	}
	
	public String getShortText() {
		return news.getShortText();
	}
	
	public void setShortText(String shortText) {
		news.setShortText(shortText);
	}
	
	public String getFullText() {
		return news.getFullText();
	}
	
	public void setFullText(String fullText) {
		news.setFullText(fullText);
	}
	
	public AuthorTO getAuthor() {
		return author;
	}
	
	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

	public Date getCreationDate() {
		return news.getCreationDate();
	}

	public void setCreationDate(Date creationDate) {
		news.setCreationDate(creationDate);
	}

	public Date getModificationDate() {
		return news.getModificationDate();
	}

	public void setModificationDate(Date modificationDate) {
		news.setModificationDate(modificationDate);
	}

	public List<CommentTO> getComments() {
		return comments;
	}

	public void setComments(List<CommentTO> comments) {
		this.comments = comments;
	}

	public List<TagTO> getTags() {
		return tags;
	}

	public void setTags(List<TagTO> tags) {
		this.tags = tags;
	}
	
	public void addComment(CommentTO comment) {
		if(comments == null) {
			comments = new ArrayList<CommentTO>();
		}
		comments.add(comment);
	}
	
	public void addTag(TagTO tag) {
		if(tags == null) {
			tags = new ArrayList<TagTO>();
		}
		tags.add(tag);
	}
	
	public int getNumberOfComments() {
		return (comments != null) ? comments.size() : 0;
	}
	
	public NewsTO getNewsTO() {
		return news;
	}
	
	public void setNewsTO(NewsTO newsTo) {
		news = newsTo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof NewsVO)) {
			return false;
		}
		boolean compareFlag = true;
		NewsVO newsObj = (NewsVO) obj;
		ListEqualityComparator<TagTO> compareTags 
			= new ListEqualityComparator<TagTO>();
		ListEqualityComparator<CommentTO> compareComments 
			= new ListEqualityComparator<CommentTO>();
		
		if(news == null) {
			compareFlag = compareFlag && (newsObj.getNewsTO() == null);
		} else {
			compareFlag = compareFlag && news.equals(newsObj.getNewsTO());
		}
		
		if(author == null) {
			compareFlag = compareFlag && (newsObj.getAuthor() == null);
		} else {
			compareFlag = compareFlag && author.equals(newsObj.getAuthor());
		}
		
		compareFlag = compareFlag 
				&& compareComments.compare(comments, newsObj.getComments());
		
		compareFlag = compareFlag 
				&& compareTags.compare(tags, newsObj.getTags());
		
		return compareFlag;
	}
	
	@Override
	public int hashCode() {
		int hash = 88;
		hash += ((news == null) ? 0 : news.hashCode()) * 44;
		hash += ((author == null) ? 0 : author.hashCode()) * 55;
		hash += ((comments == null) ? 0 : comments.hashCode()) * 66;
		hash += ((tags == null) ? 0 : tags.hashCode()) * 77;
		return hash;
	}
	
	@Override
	public String toString() {
		StringBuilder res = new StringBuilder(getClass().getSimpleName());
		res.append(": " + news);
		res.append("\n" + author);
		res.append("\nComments:\n");
		if(comments == null) {
			res.append("---");
		} else if (comments.isEmpty()) {
			res.append("---");
		} else {
			for (CommentTO comment : comments) {
				res.append(comment + "\n");
			}
		}
		res.append("\nTags:\n");
		if(tags == null) {
			res.append("---");
		} else if (tags.isEmpty()) {
			res.append("---");
		} else {
			for (TagTO tag : tags) {
				res.append(tag + "\n");
			}
		}
		return new String(res);
	}
	
	public class ListEqualityComparator<T extends AbstractTO> {

		/**
		 * Determines if two Lists are equal. Lists are considered to be equal 
		 * if they contain the same number of the same elements, order may be different 
		 * (note: null and empty list are considered to be equal)
		 * @param arg0 - 1st List
		 * @param arg1 - 2nd List
		 * @return true if lists are equal, false otherwise
		 */
		public boolean compare(List<T> arg0, List<T> arg1) {
			
			if(arg0 == null || arg0.isEmpty()) {
				if(arg1 == null || arg1.isEmpty()) {
					return true;
				} else {
					return false;
				}
			} else if(arg1 == null || arg1.isEmpty()) {
				return false;
			}
					
			return arg0.size() == arg1.size() && arg0.containsAll(arg1);
		}
	}

}
