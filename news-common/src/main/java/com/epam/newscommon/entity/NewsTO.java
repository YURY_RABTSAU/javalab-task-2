package com.epam.newscommon.entity;

import java.util.Date;

/**
 * Class contains info about news (without author/comments/tags)
 * @author Yury_Rabtsau
 *
 */
public class NewsTO extends AbstractTO {

	private String title;
	private String shortText;
	private String fullText;
	private Date creationDate;
	private Date modificationDate;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof NewsTO)) {
			return false;
		}
		NewsTO n1 = (NewsTO) obj;
		boolean compareFlag = true;
		if(title == null) {
			compareFlag = compareFlag && (n1.getTitle() == null); 
		} else {
			compareFlag = compareFlag && title.equals(n1.getTitle());
		}
		
		if(shortText == null) {
			compareFlag = compareFlag && (n1.getShortText() == null); 
		} else {
			compareFlag = compareFlag && shortText.equals(n1.getShortText());
		}

		if(fullText == null) {
			compareFlag = compareFlag && (n1.getFullText() == null); 
		} else {
			compareFlag = compareFlag && fullText.equals(n1.getFullText());
		}
		
		if(creationDate == null) {
			compareFlag = compareFlag && (n1.getCreationDate() == null); 
		} else {
			compareFlag = compareFlag && creationDate.equals(n1.getCreationDate());
		}
		
		if(modificationDate == null) {
			compareFlag = compareFlag && (n1.getModificationDate() == null); 
		} else {
			compareFlag = compareFlag && modificationDate.equals(n1.getModificationDate());
		}
		
		return compareFlag && this.getId() == n1.getId();
		/*return this.getId() == n1.getId() 
				&& title.equals(n1.getTitle())
				&& shortText.equals(n1.getShortText())
				&& fullText.equals(n1.getFullText())
				&& creationDate.equals(n1.getCreationDate())
				&& modificationDate.equals(n1.getModificationDate());*/
	}
	
	@Override
	public int hashCode() {
		int hash = 12;
		hash += super.hashCode() * 100;
		hash += ((title == null) ? 0 : title.hashCode()) * 26;
		hash += ((shortText == null) ? 0 : shortText.hashCode()) * 62;
		hash += ((fullText == null) ? 0 : fullText.hashCode()) * 13;
		hash += ((creationDate == null) ? 0 : creationDate.hashCode()) * 17;
		hash += ((modificationDate == null) ? 0 : modificationDate.hashCode()) * 18;
		return hash;
	}
	
	@Override
	public String toString() {
		StringBuilder res = new StringBuilder(super.toString());
		res.append(", title = ").append(title);
		res.append(", short text = ").append(shortText);
		res.append(", full text = ").append(fullText);
		res.append(", created = ").append(creationDate);
		res.append(", modified = ").append(modificationDate);
		return new String(res);
	}

}
