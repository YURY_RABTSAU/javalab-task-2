package com.epam.newscommon.entity;

public abstract class AbstractTO {
	
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof AbstractTO) {
			return ((AbstractTO)obj).getId()==this.id;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int hash = 1;
		hash += (int)(id - (id >>> 32));
		return hash;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + ": id = " + id;
	}

}
