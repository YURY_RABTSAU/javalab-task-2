package com.epam.newscommon.entity;

import java.util.Date;

/**
 * Class contains info about comment
 * @author Yury_Rabtsau
 *
 */
public class CommentTO extends AbstractTO {
	
	private long newsId;
	private String text;
	private Date creationDate;
	
	public long getNewsId() {
		return newsId;
	}
	
	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof CommentTO)) {
			return false;
		}
		CommentTO c1 = (CommentTO) obj;
		boolean compareFlag;
		
		if(text == null) {
			compareFlag = (c1.getText() == null);
		} else {
			compareFlag = text.equals(c1.getText());
		}
		
		if(creationDate == null) {
			compareFlag = compareFlag && (c1.getCreationDate() == null);
		} else {
			compareFlag = compareFlag && creationDate.equals(c1.getCreationDate());
		}
		
		return this.getId() == c1.getId()
				&& newsId == c1.getNewsId()
				&& compareFlag;
	}
	
	@Override
	public int hashCode() {
		int hash = 86;
		hash += super.hashCode() * 62;
		hash += ((text == null) ? 0 : text.hashCode()) * 578;
		hash += ((creationDate == null) ? 0 : creationDate.hashCode()) * 95;
		hash += (int)(newsId - (newsId >>> 32 )) * 5;
		return hash;
	}
	
	@Override
	public String toString() {
		StringBuilder res = new StringBuilder(super.toString());
		res.append(", newsId = ").append(newsId);
		res.append(", text = ").append(text);
		res.append(", created = ").append(creationDate);
		return new String(res);
	}

}
