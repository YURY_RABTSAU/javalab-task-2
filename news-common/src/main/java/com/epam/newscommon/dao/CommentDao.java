package com.epam.newscommon.dao;

import java.util.List;

import com.epam.newscommon.entity.CommentTO;
import com.epam.newscommon.exception.DaoException;

/**
 * Interface for database access to Comment
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public interface CommentDao {

	/**
	 * Finds the comment by its id
	 * @param id - comment's id
	 * @return CommentTO containing found comment or null if nothing found
	 * @throws DaoException if impossible to complete the operation
	 */
	public CommentTO getCommentById(long id) throws DaoException;
	
	/**
	 * Finds comments written to the certain news
	 * @param newsId - id of the news
	 * @return List containing all found comments (empty List if nothing found)
	 * @throws DaoException if impossible to complete the operation
	 */
	public List<CommentTO> getCommentsByNews(long newsId) throws DaoException;
	
	/**
	 * Inserts new comment record into database
	 * @param comment - data to be inserted, its id will be changed
	 * @return id of the inserted comment
	 * @throws DaoException if impossible to complete the operation
	 */
	public long createComment(CommentTO comment) throws DaoException;
	
	/**
	 * Updates comment record in the database
	 * @param comment - data to be updated
	 * @throws DaoException if impossible to complete the operation
	 */
	public void updateComment(CommentTO comment) throws DaoException;
	
	/**
	 * Deletes comment from the database
	 * @param commentId - id of the record to be deleted
	 * @throws DaoException if impossible to complete the operation
	 */
	public void deleteComment(long commentId) throws DaoException;
	
}
