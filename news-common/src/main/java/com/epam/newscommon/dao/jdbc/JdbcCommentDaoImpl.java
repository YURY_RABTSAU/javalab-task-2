package com.epam.newscommon.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.newscommon.dao.CommentDao;
import com.epam.newscommon.entity.CommentTO;
import com.epam.newscommon.exception.DaoException;

/**
 * JDBC implementation of CommentDao 
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public class JdbcCommentDaoImpl implements CommentDao {
	
	private static final Logger LOG = Logger.getLogger(JdbcCommentDaoImpl.class);
	private DataSource dataSource;
	
	
	private static final String SQL_GET_BY_ID =
			"select comment_id, comment_text, creation_date, news_id from comments where comment_id=? ";
	private static final String SQL_GET_BY_NEWS_ID =
			"select comment_id, comment_text, creation_date, news_id from comments where news_id=? order by creation_date";
	private static final String SQL_CREATE_COMMENT = 
			"insert into comments (comment_id, comment_text, creation_date, news_id) values (COMMENT_ID_SEQ.NEXTVAL, ?, ?, ?) ";
	private static final String SQL_UPDATE_COMMENT = 
			"update comments set comment_text=?, creation_date=?, news_id=? where comment_id=? ";
	private static final String SQL_DELETE_COMMENT = 
			"delete from comments where comment_id=? ";
	
	private static final String COLUMN_COMMENT_ID = "comment_id";
	private static final String COLUMN_NEWS_ID = "news_id";
	private static final String COLUMN_TEXT = "comment_text";
	private static final String COLUMN_CREATION_DATE = "creation_date";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public CommentTO getCommentById(long id) throws DaoException {
		
		LOG.debug("getCommentById start, id: " + id); 
		CommentTO result = null;
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_GET_BY_ID);
			ps.setLong(1, id);
			
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			if(rs.next()) {
				LOG.debug("resultSet is not empty");
				result = parseResultSetRow(rs);
			}
					
		} catch (SQLException e) {
			LOG.warn("SQLException in method getCommentById", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}
		
		LOG.debug("getCommentById end");
		return result;
	}
	
	@Override
	public List<CommentTO> getCommentsByNews(long newsId) throws DaoException {
		
		LOG.debug("getCommentsByNews start, news id: " + newsId);
		List<CommentTO> result = new ArrayList<CommentTO>();
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_GET_BY_NEWS_ID);
			ps.setLong(1, newsId);
			
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			while(rs.next()) {
				CommentTO comment = parseResultSetRow(rs);
				result.add(comment);
			}
					
		} catch (SQLException e) {
			LOG.warn("SQLException in method getCommentsByNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}
		
		LOG.debug("getCommentByNews end");
		return result;
	}

	@Override
	public long createComment(CommentTO comment) throws DaoException {
		
		LOG.debug("createComment start");
		long newId = -1;
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rsId = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_CREATE_COMMENT,
					new String[] {COLUMN_COMMENT_ID});
			
			ps.setString(1, comment.getText());
			Timestamp time = new Timestamp(comment.getCreationDate().getTime());
			ps.setTimestamp(2, time);
			ps.setLong(3, comment.getNewsId());
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
			rsId = ps.getGeneratedKeys();
			if(rsId.next()) {
				newId = rsId.getLong(1);
				comment.setId(newId);
			} else {
				LOG.warn("Can't get generated id");
			}
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method createComment", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rsId);
		}
		
		LOG.debug("createComment end, id=" + newId);
		return newId;
		
	}

	@Override
	public void updateComment(CommentTO comment) throws DaoException {
		
		LOG.debug("updateComment start");
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_UPDATE_COMMENT);
			ps.setString(1, comment.getText());
			Timestamp time = new Timestamp(comment.getCreationDate().getTime());
			ps.setTimestamp(2, time);
			ps.setLong(3, comment.getNewsId());
			ps.setLong(4, comment.getId());
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method updateComment", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}
		
		LOG.debug("updateComment end");
		
	}

	@Override
	public void deleteComment(long commentId) throws DaoException {
		
		LOG.debug("deleteComment start, id=" + commentId);
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_DELETE_COMMENT);
			ps.setLong(1, commentId);
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method deleteComment", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}
		
		LOG.debug("deleteComment end");
		
	}
	
	private CommentTO parseResultSetRow(ResultSet rs) throws SQLException {
		CommentTO comment = new CommentTO();
		comment.setId(rs.getLong(COLUMN_COMMENT_ID));
		comment.setNewsId(rs.getLong(COLUMN_NEWS_ID));
		comment.setText(rs.getString(COLUMN_TEXT));
		comment.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
		return comment;
	}

}
