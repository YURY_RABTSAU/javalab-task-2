package com.epam.newscommon.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.newscommon.dao.AuthorDao;
import com.epam.newscommon.entity.AuthorTO;
import com.epam.newscommon.exception.DaoException;

/**
 * JDBC implementation of AuthorDao 
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public class JdbcAuthorDaoImpl implements AuthorDao {
	
	private static final Logger LOG = Logger.getLogger(JdbcAuthorDaoImpl.class);
	private DataSource dataSource;
	
	private static final String SQL_GET_BY_ID =
			"select author_id, name, expired from author where author_id=? ";
	private static final String SQL_GET_BY_NAME =
			"select author_id, name, expired from author where name=? ";
	private static final String SQL_GET_BY_NEWS_ID =
			"select author_id, name, expired from author where author_id in (select author_id from news_author where news_id=? ) ";
	private static final String SQL_GET_ALL_AUTHORS =
			"select author_id, name, expired from author order by name";
	private static final String SQL_CREATE_AUTHOR = 
			"insert into author (author_id, name, expired) values (AUTHOR_ID_SEQ.NEXTVAL, ?, ?) ";
	private static final String SQL_UPDATE_AUTHOR = 
			"update author set name=?, expired=? where author_id=? ";
	private static final String SQL_DELETE_AUTHOR = 
			"delete from author where author_id=? ";
	private static final String CREATE_NEWS_AUTHOR_SQL = 
			"insert into news_author (author_id, news_id) values (?, ?) ";
	private static final String DELETE_NEWS_AUTHOR_SQL = 
			"delete from news_author where author_id=? and news_id=? ";
	
	private static final String COLUMN_AUTHOR_ID = "author_id";
	private static final String COLUMN_AUTHOR_NAME = "name";
	private static final String COLUMN_EXPIRED = "expired";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public AuthorTO getAuthorById(long id) throws DaoException {
		
		LOG.debug("getAuthorById start, id: " + id); 
		AuthorTO result = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_GET_BY_ID);
			ps.setLong(1, id);
			
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			if(rs.next()) {
				LOG.debug("resultSet is not empty");
				result = parseResultSetRow(rs);
			}
			
					
		} catch (SQLException e) {
			LOG.warn("SQLException in method getAuthorById", e);
			throw new DaoException("SQLException in method getAuthorById", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}
		
		LOG.debug("getAuthorById end");
		return result;
	}

	@Override
	public AuthorTO getAuthorByName(String name) throws DaoException {
		
		LOG.debug("getAuthorByName start, name: " + name); 
		AuthorTO result = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_GET_BY_NAME);
			ps.setString(1, name);
			
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			if(rs.next()) {
				LOG.debug("resultSet is not empty");
				result = parseResultSetRow(rs);
			}
					
		} catch (SQLException e) {
			LOG.warn("SQLException in method getAuthorByName", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}
		
		LOG.debug("getAuthorByName end");
		return result;
	}

	@Override
	public AuthorTO getAuthorByNews(long newsId) throws DaoException {
		
		LOG.debug("getAuthorByNewsId start, news id: " + newsId); 
		AuthorTO result = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_GET_BY_NEWS_ID);
			ps.setLong(1, newsId);
			
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			if(rs.next()) {
				LOG.debug("resultSet is not empty");
				result = parseResultSetRow(rs);
			}
					
		} catch (SQLException e) {
			LOG.warn("SQLException in method getAuthorByNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}
		
		LOG.debug("getAuthorByNewsId end");
		return result;
	}
	
	@Override
	public List<AuthorTO> getAllAuthors() throws DaoException {
		
		LOG.debug("getAllAuthors start");
		List<AuthorTO> resultList = new ArrayList<AuthorTO>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQL_GET_ALL_AUTHORS);
			while(rs.next()) {
				AuthorTO author = parseResultSetRow(rs);
				resultList.add(author);
			}
		} catch (SQLException e) {
			LOG.warn("SQLException in method getAllAuthors", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, st, rs);
		}
		LOG.debug("getAllAuthors end");
		return resultList;
	};

	@Override
	public long createAuthor(AuthorTO author) throws DaoException {
		
		LOG.debug("createAuthor start");
		long newId = -1;
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rsId = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_CREATE_AUTHOR, 
					new String[] {COLUMN_AUTHOR_ID});
			
			
			
			
			ps.setString(1, author.getName());
			Date expiredDate = author.getExpired();
			if (expiredDate != null) {
				Timestamp time = new Timestamp(expiredDate.getTime());
				ps.setTimestamp(2, time);
			} else {
				ps.setNull(2, Types.TIMESTAMP);
			}
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
			rsId = ps.getGeneratedKeys();
			if(rsId.next()) {
				newId = rsId.getLong(1);
				author.setId(newId);
			} else {
				LOG.warn("Can't get generated id");
			}
			
			
			
		} catch (SQLException e) {
			LOG.error("SQLException in method createAuthor", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rsId);
		}
		
		LOG.debug("createAuthor end, id=" + newId);
		return newId;
		
	}

	@Override
	public void updateAuthor(AuthorTO author) throws DaoException {
		
		LOG.debug("updateAuthor start");
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			LOG.debug("before getConnection");
			con = dataSource.getConnection();
			LOG.debug("after getConnection");
			ps = con.prepareStatement(SQL_UPDATE_AUTHOR);
			LOG.debug("after prepareStatement");
			
			ps.setString(1, author.getName());
			Date expiredDate = author.getExpired();
			if (expiredDate != null) {
				Timestamp time = new Timestamp(expiredDate.getTime());
				ps.setTimestamp(2, time);
			} else {
				ps.setNull(2, Types.TIMESTAMP);
			}
			
			ps.setLong(3, author.getId());
			
			LOG.debug("before executeUpdate");
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method updateAuthor", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}
		
		LOG.debug("updateAuthor end");
		
	}

	@Override
	public void deleteAuthor(long authorId) throws DaoException {
		
		LOG.debug("deleteAuthor start, id=" + authorId);
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_DELETE_AUTHOR);
			ps.setLong(1, authorId);
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method deleteAuthor", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}
		
		LOG.debug("deleteAuthor end");
	}
	
	@Override
	public void createNewsAuthor(long newsId, long authorId) throws DaoException {
		
		LOG.debug("createNewsAuthor start");
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(CREATE_NEWS_AUTHOR_SQL);
			ps.setLong(1, authorId);
			ps.setLong(2, newsId);
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method createNewsAuthor", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}
		
		LOG.debug("createNewsAuthor end");
	}

	@Override
	public void deleteNewsAuthor(long newsId, long authorId) throws DaoException {
		
		LOG.debug("deleteNewsAuthor start");

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(DELETE_NEWS_AUTHOR_SQL);
			ps.setLong(1, authorId);
			ps.setLong(2, newsId);
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method deleteNewsAuthor", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}
		
		LOG.debug("deleteNewsAuthor end");
	}
	
	private AuthorTO parseResultSetRow(ResultSet rs) throws SQLException {
		AuthorTO author = new AuthorTO();
		author.setId(rs.getLong(COLUMN_AUTHOR_ID));
		author.setName(rs.getString(COLUMN_AUTHOR_NAME));
		author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
		return author;
	}

}
