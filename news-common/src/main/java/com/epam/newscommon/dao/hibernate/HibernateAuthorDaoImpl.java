package com.epam.newscommon.dao.hibernate;

import java.util.List;

import com.epam.newscommon.dao.AuthorDao;
import com.epam.newscommon.entity.AuthorTO;
import com.epam.newscommon.exception.DaoException;

public class HibernateAuthorDaoImpl implements AuthorDao {

	@Override
	public AuthorTO getAuthorById(long id) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AuthorTO getAuthorByName(String name) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AuthorTO getAuthorByNews(long newsId) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuthorTO> getAllAuthors() throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long createAuthor(AuthorTO author) throws DaoException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateAuthor(AuthorTO author) throws DaoException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAuthor(long authorId) throws DaoException {
		// TODO Auto-generated method stub

	}

	@Override
	public void createNewsAuthor(long newsId, long authorId)
			throws DaoException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteNewsAuthor(long newsId, long authorId)
			throws DaoException {
		// TODO Auto-generated method stub

	}

}
