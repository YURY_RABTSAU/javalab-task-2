package com.epam.newscommon.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.entity.NewsTO;
import com.epam.newscommon.entity.SearchCriteria;
import com.epam.newscommon.exception.DaoException;

/**
 * JDBC implementation of NewsDao
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public class JdbcNewsDaoImpl implements NewsDao {

	private static Logger LOG = Logger.getLogger(JdbcNewsDaoImpl.class);
	private DataSource dataSource;

	private static final String GET_BY_ID_SQL = "select news_id, title, short_text, full_text, creation_date, modification_date"
			+ " from news where news_id=? ";
	private static final String CREATE_NEWS_SQL = "insert into news (news_id, title, short_text, full_text, creation_date, modification_date) values (NEWS_ID_SEQ.NEXTVAL, ?, ?, ?, ?, ?) ";
	private static final String UPDATE_NEWS_SQL = "update news set title=?, short_text=?, full_text=?, creation_date=?, modification_date=? where news_id=? ";
	private static final String DELETE_NEWS_SQL = "delete from news where news_id=? ";

	private static final String ORDER_BY_COMMENTS_SQL = "ORDER BY (select count(comment_id) FROM comments WHERE news.news_id = comments.news_id) DESC";

	private static final String COLUMN_NEWS_ID = "news_id";
	private static final String COLUMN_TITLE = "title";
	private static final String COLUMN_SHORT_TEXT = "short_text";
	private static final String COLUMN_FULL_TEXT = "full_text";
	private static final String COLUMN_CREATION_DATE = "creation_date";
	private static final String COLUMN_MODIFICATION_DATE = "modification_date";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public NewsTO getNewsById(long id) throws DaoException {

		NewsTO result = null;
		LOG.debug("getNewsById start, id: " + id);

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(GET_BY_ID_SQL);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			if (rs.next()) {
				LOG.debug("resultSet is not empty");
				result = parseResultSetRow(rs);
			}

		} catch (SQLException e) {
			LOG.warn("SQLException in method getNewsById", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}

		LOG.debug("getNewsById end");
		return result;
	}

	@Override
	@Deprecated
	public List<NewsTO> getNewsByAuthor(long authorId) throws DaoException {

		LOG.debug("getNewsByAuthor start, authorId: " + authorId);
		List<NewsTO> result = new ArrayList<NewsTO>();

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String query = new QueryBuilder().getSearchQuery(0, true, false,
					false);
			ps = con.prepareStatement(query);
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			while (rs.next()) {
				NewsTO news = parseResultSetRow(rs);
				result.add(news);
			}

		} catch (SQLException e) {
			LOG.warn("SQLException in method getNewsByAuthor", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}

		LOG.debug("getNewsByAuthor end");
		return result;
	}

	@Override
	@Deprecated
	public List<NewsTO> getNewsByTags(List<Long> tagIds) throws DaoException {

		LOG.debug("getNewsByTags start, tagIds number: " + tagIds.size());
		List<NewsTO> result = new ArrayList<NewsTO>();

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String query = new QueryBuilder().getSearchQuery(tagIds.size(),
					false, false, false);
			ps = con.prepareStatement(query);
			int index = 1;
			for (Long tagId : tagIds) {
				ps.setLong(index, tagId);
				index++;
			}
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			while (rs.next()) {
				NewsTO news = parseResultSetRow(rs);
				result.add(news);
			}

		} catch (SQLException e) {
			LOG.warn("SQLException in method getNewsByTag", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}

		LOG.debug("getNewsByTag end");
		return result;
	}

	@Override
	@Deprecated
	public List<NewsTO> getNews(List<Long> tagIds, long authorId, int offset,
			int limit) throws DaoException {

		LOG.debug("getNews start");
		List<NewsTO> result = new ArrayList<NewsTO>();

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();

			if (tagIds == null) {
				tagIds = new ArrayList<Long>();
			}
			boolean needAuthor = authorId > 0;
			boolean needOffset = offset > 0;
			boolean needLimit = limit > 0;
			String query = new QueryBuilder().getSearchQuery(tagIds.size(),
					needAuthor, needOffset, needLimit);
			ps = con.prepareStatement(query);

			int index = 1;
			for (Long tagId : tagIds) {
				ps.setLong(index, tagId);
				index++;
			}
			if (needAuthor) {
				ps.setLong(index, authorId);
				index++;
			}
			if (needOffset) {
				ps.setInt(index, offset);
				index++;
			} else {
				offset = 0;
			}
			if (needLimit) {
				ps.setInt(index, offset + limit);
			}

			rs = ps.executeQuery();
			LOG.debug("execute statement");
			while (rs.next()) {
				NewsTO news = parseResultSetRow(rs);
				result.add(news);
			}

		} catch (SQLException e) {
			LOG.warn("SQLException in method getNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}

		LOG.debug("getNews end");
		return result;
	}

	@Override
	public List<NewsTO> getNews(SearchCriteria searchCriteria)
			throws DaoException {

		LOG.debug("getNewsBySearchCriteria start");
		List<NewsTO> result = new ArrayList<NewsTO>();

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();

			String query = new QueryBuilder().getSearchQuery(searchCriteria);
			ps = con.prepareStatement(query);

			int index = 1;
			if (searchCriteria.isTagSeach()) {
				for (Long tagId : searchCriteria.getTagIds()) {
					ps.setLong(index, tagId);
					index++;
				}
			}
			if (searchCriteria.isAuthorSearch()) {
				ps.setLong(index, searchCriteria.getAuthorId());
				index++;
			}
			if (searchCriteria.isOffset()) {
				ps.setInt(index, searchCriteria.getOffset());
				index++;
			}
			if (searchCriteria.isLimited()) {
				ps.setInt(index, searchCriteria.getOffsetAndLimit());
			}

			rs = ps.executeQuery();
			LOG.debug("execute statement");
			while (rs.next()) {
				NewsTO news = parseResultSetRow(rs);
				result.add(news);
			}

		} catch (SQLException e) {
			LOG.warn("SQLException in method getNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}

		LOG.debug("getNews end");
		return result;
	}

	@Override
	public Long getNextNews(SearchCriteria searchCriteria, long newsId)
			throws DaoException {
		
		LOG.debug("getNextNews start, newsId=" + newsId);
		Long result = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();

			String query = new QueryBuilder().getNextPreviousIdQuerry(
					searchCriteria, true);
			ps = con.prepareStatement(query);
			int index = 1;
			for (int i = 1; i <= 2; i++) {
				if (searchCriteria.isTagSeach()) {
					for (Long tagId : searchCriteria.getTagIds()) {
						ps.setLong(index, tagId);
						index++;
					}
				}
				if (searchCriteria.isAuthorSearch()) {
					ps.setLong(index, searchCriteria.getAuthorId());
					index++;
				}
			}
			ps.setLong(index, newsId);

			rs = ps.executeQuery();
			LOG.debug("execute statement");
			if (rs.next()) {
				LOG.debug("resultSet is not empty");
				result = rs.getLong(COLUMN_NEWS_ID);
			}

		} catch (SQLException e) {
			LOG.warn("SQLException in method getNextNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}

		LOG.debug("getNextNews end");
		return result;
	};

	@Override
	public Long getPreviousNews(SearchCriteria searchCriteria, long newsId)
			throws DaoException {
		
		LOG.debug("getNextNews start, newsId=" + newsId);
		Long result = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();

			String query = new QueryBuilder().getNextPreviousIdQuerry(
					searchCriteria, false);
			ps = con.prepareStatement(query);
			int index = 1;
			for (int i = 1; i <= 2; i++) {
				if (searchCriteria.isTagSeach()) {
					for (Long tagId : searchCriteria.getTagIds()) {
						ps.setLong(index, tagId);
						index++;
					}
				}
				if (searchCriteria.isAuthorSearch()) {
					ps.setLong(index, searchCriteria.getAuthorId());
					index++;
				}
			}
			ps.setLong(index, newsId);

			rs = ps.executeQuery();
			LOG.debug("execute statement");
			if (rs.next()) {
				LOG.debug("resultSet is not empty");
				result = rs.getLong(COLUMN_NEWS_ID);
			}

		} catch (SQLException e) {
			LOG.warn("SQLException in method getNextNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}

		LOG.debug("getNextNews end");
		return result;
	};

	@Override
	public long createNews(NewsTO news) throws DaoException {

		LOG.debug("createNews start");
		long newId = -1;

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rsId = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(CREATE_NEWS_SQL,
					new String[] { COLUMN_NEWS_ID });

			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			Timestamp creationDate = new Timestamp(news.getCreationDate()
					.getTime());
			ps.setTimestamp(4, creationDate);
			java.sql.Date modificationDate = new java.sql.Date(news
					.getModificationDate().getTime());
			ps.setDate(5, modificationDate);

			ps.executeUpdate();
			LOG.debug("execute statement");

			rsId = ps.getGeneratedKeys();
			if (rsId.next()) {
				newId = rsId.getLong(1);
				news.setId(newId);
			}

		} catch (SQLException e) {
			LOG.warn("SQLException in method createNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rsId);
		}

		LOG.debug("createNews end, id=" + newId);
		return newId;
	}

	@Override
	public void updateNews(NewsTO news) throws DaoException {

		LOG.debug("updateNews start");

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(UPDATE_NEWS_SQL);

			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			Timestamp creationDate = new Timestamp(news.getCreationDate()
					.getTime());
			ps.setTimestamp(4, creationDate);
			java.sql.Date modificationDate = new java.sql.Date(news
					.getModificationDate().getTime());
			ps.setDate(5, modificationDate);
			ps.setLong(6, news.getId());

			ps.executeUpdate();
			LOG.debug("execute statement");

		} catch (SQLException e) {
			LOG.warn("SQLException in method updateNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}

		LOG.debug("updateNews end");
	}

	@Override
	public void deleteNews(long newsId) throws DaoException {

		LOG.debug("deleteNews start");

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(DELETE_NEWS_SQL);
			ps.setLong(1, newsId);

			ps.executeUpdate();
			LOG.debug("execute statement");

		} catch (SQLException e) {
			LOG.warn("SQLException in method deleteNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}

		LOG.debug("deleteNews end");
	}

	private NewsTO parseResultSetRow(ResultSet rs) throws SQLException {
		NewsTO news = new NewsTO();
		news.setId(rs.getLong(COLUMN_NEWS_ID));
		news.setTitle(rs.getString(COLUMN_TITLE));
		news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
		news.setFullText(rs.getString(COLUMN_FULL_TEXT));
		news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
		news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
		return news;
	}

	private class QueryBuilder {

		private static final String BASE = " select news_id, title, short_text, full_text, creation_date, modification_date "
				+ " from news ";
		private static final String SEARCH_BY_TAG = " news_id in (select news_id from news_tag where tag_id=? ) ";
		private static final String SEARCH_BY_AUTHOR = " news_id in (select news_id from news_author where author_id=? ) ";
		private static final String OUTER_LIST_BEFORE = " select news_id, title, short_text, full_text, creation_date, modification_date "
				+ " from ( select table_a.* , ROWNUM as rn from ( ";
		private static final String OUTER_LIST_AFTER = " ) table_a ) where ";

		private static final String INNER_PREV_NEXT_BEFORE = " select news_id , ROWNUM as rn from ( ";
		private static final String INNER_PREV_NEXT_AFTER = " ) ";
		private static final String OUTER_PREV_NEXT_BEFORE = "select news_id from ( ";
		private static final String OUTER_PREV_NEXT_MID_1 = " ) where rn ";
		private static final String OUTER_PREV_NEXT_MID_2 = " 1 = ( select rn from ( ";
		private static final String OUTER_PREV_NEXT_AFTER = " ) where news_id = ? ) ";

		//private static final String SEARCH_CONDITION_OPERATOR = " AND ";
		private static final String SEARCH_CONDITION_OPERATOR = " OR ";
		
		@Deprecated
		public String getSearchQuery(int tagCount, boolean author,
				boolean offset, boolean limit) {
			throw new UnsupportedOperationException();
		}

		public String getSearchQuery(SearchCriteria criteria) {

			StringBuilder innerQuery = getInnerQuerry(criteria);

			StringBuilder outerQuerry;
			if (criteria.isOffset() || criteria.isLimited()) {
				outerQuerry = new StringBuilder(OUTER_LIST_BEFORE);
				outerQuerry.append(innerQuery).append(OUTER_LIST_AFTER);
				if (criteria.isOffset() && criteria.isLimited()) {
					outerQuerry.append(" rn > ? and rn < ? ");
				} else if (criteria.isOffset()) {
					outerQuerry.append(" rn > ? ");
				} else {
					outerQuerry.append(" rn < ? ");
				}
			} else {
				outerQuerry = innerQuery;
			}

			LOG.debug("Querry generated: " + outerQuerry);

			return new String(outerQuerry);

		}

		private StringBuilder getInnerQuerry(SearchCriteria criteria) {

			StringBuilder innerQuery = new StringBuilder(BASE);
			boolean isFirstWhereCondition = true;

			if (criteria.isTagSeach()) {
				isFirstWhereCondition = appendWhereConditionPrefix(
						isFirstWhereCondition, innerQuery, " WHERE ", SEARCH_CONDITION_OPERATOR);
				innerQuery.append(SEARCH_BY_TAG);
				for (int i = 2; i <= criteria.getNumberOfTags(); i++) {
					innerQuery.append(SEARCH_CONDITION_OPERATOR).append(SEARCH_BY_TAG);
				}
			}

			if (criteria.isAuthorSearch()) {
				isFirstWhereCondition = appendWhereConditionPrefix(
						isFirstWhereCondition, innerQuery, " WHERE ", SEARCH_CONDITION_OPERATOR);
				innerQuery.append(SEARCH_BY_AUTHOR);
			}
			innerQuery.append(ORDER_BY_COMMENTS_SQL);

			return innerQuery;
		}

		private boolean appendWhereConditionPrefix(
				boolean isFirstWhereCondition, StringBuilder query,
				String whereStatement, String conditionOperator) {
			//
			if (isFirstWhereCondition) {
				query.append(whereStatement);
			} else {
				query.append(conditionOperator);
			}
			return false;
		}

		public String getNextPreviousIdQuerry(SearchCriteria criteria,
				boolean isNext) {
			StringBuilder innerPrevNextQuery = new StringBuilder(
					INNER_PREV_NEXT_BEFORE);
			innerPrevNextQuery.append(getInnerQuerry(criteria));
			innerPrevNextQuery.append(INNER_PREV_NEXT_AFTER);

			StringBuilder outerQuerry;
			outerQuerry = new StringBuilder(OUTER_PREV_NEXT_BEFORE);
			outerQuerry.append(innerPrevNextQuery);
			outerQuerry.append(OUTER_PREV_NEXT_MID_1);
			outerQuerry.append(isNext ? "-" : "+");
			outerQuerry.append(OUTER_PREV_NEXT_MID_2);
			outerQuerry.append(innerPrevNextQuery);
			outerQuerry.append(OUTER_PREV_NEXT_AFTER);

			LOG.debug("Querry generated: " + outerQuerry);

			return new String(outerQuerry);

		}
	}

}
