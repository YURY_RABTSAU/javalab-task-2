<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="./resources/js/news.js" ></script>
<title><spring:message code="title.newsDetail" /></title>
</head>
<body>

	<a href="./news" class="btn btn-default button-back">← <spring:message code="body.back" /></a>

	<!-- news -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="news-title panel-title">${newsVO.title}</div>
			<div class="author">
				(<spring:message code="body.author" /> ${newsVO.author.name})
			</div>
			
			<div class="tags">
				<spring:message code="body.tags" />
				<c:forEach var="tag" items="${newsVO.tags}">
					<c:out value=" ${tag.name}" />,
				</c:forEach>
			</div>
			
			<div class="modification-date">
				<spring:message code="body.modified" />:
				<spring:message code="body.datePattern" var="datePattern" />

				<fmt:formatDate pattern="${datePattern}"
					value="${newsVO.modificationDate}" />
			</div>
		</div>
		<!-- /news header -->
		
		<div class="news-full-text panel-body">
			<pre><c:out value="${newsVO.fullText}" /></pre>
		</div>
	</div>
	<!-- /news -->
	
	<!-- prev/next -->
	<div>
		<ul class="pager">
			<li class="previous 
				<c:if test="${prevId == null}">disabled</c:if>
			">
				<a href="?id=${prevId}">
					<spring:message code="body.prevNews" />
				</a>
			</li>
			<li class="next
				<c:if test="${nextId == null}">disabled</c:if>
			">
				<a href="?id=${nextId}">
					<spring:message code="body.nextNews" />
				</a>
			</li>
		</ul>
	</div>
	<!-- /prev/next -->

	<!-- all comments -->
	<div class="panel panel-default col-xs-7">
		<div class="panel-heading">
			<spring:message code="body.comments" />
			:
		</div>
		<div class="panel-body">
			<c:forEach var="comment" items="${newsVO.comments}">
				<!-- single comment -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<spring:message code="body.dateTimePattern" 
							var="dateTimePattern" />
						<fmt:formatDate pattern="${dateTimePattern}"
							value="${comment.creationDate}" />
					</div>
					<div class="panel-body">
						<c:out value="${comment.text}" />
					</div>
				</div>
				<!-- /single comment -->
			</c:forEach>
			
			<!-- new comment -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<spring:message code="body.newComment" />
				</div>
				<div class="new-comment-form panel-body">
					<form action="./comment" method="post">
						<input type="hidden" name="newsId" value="${newsVO.id}">
						<textarea name="commentText" required maxlength="100" 
							placeholder="<spring:message code="body.typeHere" />"
							></textarea>
						<button class="btn btn-primary" type="submit">
							<span class="glyphicon glyphicon-ok"></span> |
							<spring:message code="body.submitComment" />
						</button>
					</form>
				</div>
			</div><!-- /new comment -->
			
		</div>
	</div>
	<!-- /all comments -->


</body>
</html>