<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="./resources/js/header.js" ></script>
</head>
<body>

	<div id="header">
		<h1><spring:message code="header.title" /></h1>
	</div>
	
		<div id="locale-container" class="btn-group" role="group"> 
			<button type="button" class="btn btn-default" 
				onclick="setLocale('en')" >en</button> 
			
			<button type="button" class="btn btn-default" 
				onclick="setLocale('ru')" >ru</button> 
			<a id="locale-link" href="#"></a>
		</div>
</body>
</html>