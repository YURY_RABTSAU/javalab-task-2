<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css"
	href="./resources/css/root.css">
	
<script type="text/javascript" src="./resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="./resources/js/bootstrap.js"></script>
<script type="text/javascript"
	src="./resources/js/dropdowns-enhancement.js"></script>

</head>
<body>

	
	<div id="outer-container" class="navbar navbar-default">
		<div id="header-container" class="container-fluid navbar navbar-fixed-top">
			<tiles:insertAttribute name="header" />
		</div>
		<div id="content-container" class="container">
		<div class="row">
			<div id="body-container" class="col-xs-12">
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		</div>
		<div id="footer-container" class="navbar navbar-fixed-bottom row-fluid">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>

</body>
</html>