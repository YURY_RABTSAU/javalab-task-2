/**
 * 
 */
function clickSelectedElements() {
	var elems = document.getElementsByClassName('search-input-selected');
	for(var i=0; i<elems.length; i++) {
		elems[i].click();
	}
	if(elems.length == 0) {
		$("input[id*='author-button-']")[0].click();
	}
}

function disableDeadLinks() {
	$("li.disabled a").each(function(index, elem) {
		this.href="";
	});
}

window.onload=function() {
	disableDeadLinks();
	clickSelectedElements();
}
