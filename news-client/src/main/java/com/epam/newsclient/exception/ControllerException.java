package com.epam.newsclient.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)//for 500
public class ControllerException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ControllerException() {
		super();
	}
	
	public ControllerException(String s) {
		super(s);
	}
	
	public ControllerException(Throwable t) {
		super(t);
	}
	
	public ControllerException(String s, Throwable t) {
		super(s, t);
	}
	
}
