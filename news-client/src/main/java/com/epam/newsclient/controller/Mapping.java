package com.epam.newsclient.controller;


public class Mapping {

	public static final String ROOT = "/";
	public static final String HOME = "/home";
	public static final String NEWS = "/news";
	public static final String NEWS_DETAIL = "/newsdetail";
	public static final String SEARCH = "/search";
	public static final String SEND_COMMENT = "/comment";
	
	public static final String ERROR_404 = "/404";
	public static final String ERROR_500 = "/500";
	

}
