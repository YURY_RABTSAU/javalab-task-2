package com.epam.newsclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.newscommon.service.AuthorService;
import com.epam.newscommon.service.TagService;

@Controller
public class ErrorController {
	
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;

	@RequestMapping(value=Mapping.ERROR_404)
	public String error404(Model model) throws Exception {
		
		return "404View";
	}
	
	@RequestMapping(value=Mapping.ERROR_500)
	public String error500(Model model) throws Exception {
		
		return "500View";
	}

}
